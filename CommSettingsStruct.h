struct CommSettingsStruct;
#pragma once
#include "StreamingMessageStruct.h"

struct CommSettingsStruct
{
public:
	// Maps neighbour processes numbers to set of messages that are supposed to be sent to them / received from them
	std::map<int, std::vector<StreamingMessageStruct>> MessagesToSend;
	std::map<int, std::vector<StreamingMessageStruct>> MessagesToReceive;

};
