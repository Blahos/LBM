struct GridDirectionStruct;
#pragma once

enum BaseDirection
{
	Down = (int)-1,
	Straight = (int)0,
	Up = (int)1
};

struct GridDirectionStruct
{
public :
	GridDirectionStruct() { }
	GridDirectionStruct(BaseDirection aX, BaseDirection aY, BaseDirection aZ)
		:X(aX), Y(aY), Z(aZ) { }
	GridDirectionStruct(int aX, int aY, int aZ)
		:GridDirectionStruct((BaseDirection)aX, (BaseDirection)aY, (BaseDirection)aZ) { }

	BaseDirection X = BaseDirection::Straight;
	BaseDirection Y = BaseDirection::Straight;
	BaseDirection Z = BaseDirection::Straight;

	bool operator==(const GridDirectionStruct& aOther) const
	{
		return X == aOther.X && Y == aOther.Y && Z == aOther.Z;
	}
	bool operator<(const GridDirectionStruct& aOther) const
	{
		return X < aOther.X || (X == aOther.X && Y < aOther.Y) || (X == aOther.X && Y == aOther.Y && Z < aOther.Z);
	}
};
