#pragma once
#include <map>
#include <string>

enum LatticeTypeEnum
{
	D2Q9  =  9,
	D3Q15 = 15,
	D3Q19 = 19,
	D3Q27 = 27,
};

static const std::map<int, LatticeTypeEnum> IntToLatticeType =
{
		{9,  LatticeTypeEnum::D2Q9 },
		{15, LatticeTypeEnum::D3Q15},
		{19, LatticeTypeEnum::D3Q19},
		{27, LatticeTypeEnum::D3Q27}
};

static const std::map<LatticeTypeEnum, std::string> LatticeTypeToLatticeName =
{
		{LatticeTypeEnum::D2Q9 , "D2Q9" },
		{LatticeTypeEnum::D3Q15, "D3Q15"},
		{LatticeTypeEnum::D3Q19, "D3Q19"},
		{LatticeTypeEnum::D3Q27, "D3Q27"}
};
