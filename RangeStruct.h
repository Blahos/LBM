struct RangeStruct;
#pragma once
#include "GridDirectionStruct.h"

struct RangeStruct
{
public:
	int StartX;
	int EndX;
	int StartY;
	int EndY;
	int StartZ;
	int EndZ;

	RangeStruct(int aStartX, int aEndX, int aStartY, int aEndY, int aStartZ, int aEndZ)
		:StartX(aStartX), EndX(aEndX), StartY(aStartY), EndY(aEndY), StartZ(aStartZ), EndZ(aEndZ) { }
	RangeStruct() : RangeStruct(-1, -1, -1, -1, -1, -1) { }

	RangeStruct Shift(const GridDirectionStruct& aDir);
};
