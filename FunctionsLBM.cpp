#include "FunctionsLBM.h"

void SetInitialStateFromExpression(LBM_REAL* const (&aArray),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig, const std::vector<std::string>& aExpressionStrings)
{
	assert(aExpressionStrings.size() == 4);
	if (aExpressionStrings.size() != 4)
	{
		std::cout<<"Expression count for initial condition must be 4 !"<<std::endl;
		exit(1);
	}

	// LBM_REAL lLatticeVelocity = 1.0; The simulation is set so the lattice velocity is always 1

	std::vector<exprtk::expression<LBM_REAL>> lExpressions;

	for (int iExp = 0; iExp < aExpressionStrings.size(); iExp++)
	{
		exprtk::expression<LBM_REAL> lExpression = CreateStandardExpression(aExpressionStrings.at(iExp));

		// Example: GridSizeX = 51 => nodes have positions 0, 1, 2, ..., 50
		// Therefore, the last node has its index position equal to 50.
		// This is what we use as SizeX variable
		lExpression.get_symbol_table(0).get_variable(SizeXString)->ref() = aConfig.GridSizeX - 1;
		lExpression.get_symbol_table(0).get_variable(SizeYString)->ref() = aConfig.GridSizeY - 1;
		lExpression.get_symbol_table(0).get_variable(SizeZString)->ref() = aConfig.GridSizeZ - 1;
		lExpressions.push_back(lExpression);
	}

	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ; iZ <= aThreadWorkRange.EndZ; iZ++)
			{
				for (int iExp = 0; iExp < aExpressionStrings.size(); iExp++)
				{
					lExpressions[iExp].get_symbol_table(0).get_variable(XString)->ref() = iX;
					lExpressions[iExp].get_symbol_table(0).get_variable(YString)->ref() = iY;
					lExpressions[iExp].get_symbol_table(0).get_variable(ZString)->ref() = iZ;
				}

				LBM_REAL lXVelocity = lExpressions[0].value();
				LBM_REAL lYVelocity = lExpressions[1].value();
				LBM_REAL lZVelocity = lExpressions[2].value();
				LBM_REAL lDensity = lExpressions[3].value();

				for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
				{
					int lIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);
					Direction* lDir = &LatticeTypeToDirections.at(aConfig.LatticeType)[iDir];
//					lXVelocity *= sin(iX / lXSize * 2 * PI);

					LBM_REAL lVelDotDir = lXVelocity * lDir->X + lYVelocity * lDir->Y + lZVelocity * lDir->Z;
					LBM_REAL lVelDotVel = lXVelocity * lXVelocity + lYVelocity * lYVelocity + lZVelocity * lZVelocity;

					LBM_REAL lFEq = lDensity * lDir->Weight *
							(
									1 +
									3.0 * lVelDotDir +
									9.0/2 * lVelDotDir * lVelDotDir +
									-3.0/2 * lVelDotVel
							);

					aArray[lIndex] = lFEq;
				}
			}
		}
	}
}
void SetMacroValues(LBM_REAL* const aDensity,
		LBM_REAL* const aVelocityX, LBM_REAL* const aVelocityY, LBM_REAL* const aVelocityZ,
		const LBM_REAL* const aWorkArray, const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig)
{
	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ; iZ <= aThreadWorkRange.EndZ; iZ++)
			{
				int lMacroIndex = GetMacroArrayIndex(iX, iY, iZ, aProcessWorkRange);
				int lNodeTypeIndex = GetNodesTypeArrayIndex(iX, iY, iZ, aProcessWorkRange);

				aVelocityX[lMacroIndex] = 0;
				aVelocityY[lMacroIndex] = 0;
				aVelocityZ[lMacroIndex] = 0;

				aDensity[lMacroIndex] = 0;

				if (aNodesType[lNodeTypeIndex] == LatticeNodeTypeEnum::SolidWall) continue;

				for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
				{
					int lIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);
					Direction* lDir = &LatticeTypeToDirections.at(aConfig.LatticeType)[iDir];

					aVelocityX[lMacroIndex] += lDir->X * aWorkArray[lIndex];
					aVelocityY[lMacroIndex] += lDir->Y * aWorkArray[lIndex];
					aVelocityZ[lMacroIndex] += lDir->Z * aWorkArray[lIndex];

					aDensity[lMacroIndex] += aWorkArray[lIndex];
				}
				aVelocityX[lMacroIndex] /= aDensity[lMacroIndex];
				aVelocityY[lMacroIndex] /= aDensity[lMacroIndex];
				aVelocityZ[lMacroIndex] /= aDensity[lMacroIndex];
			}
		}
	}
}

void SpecifyNodesTypeFromExpression(LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig, const std::string& aExpressionString)
{
	// Threads "at the borders" will also specify the node type in the extension layers
	int lStartX = aThreadWorkRange.StartX;
	if (lStartX == aProcessWorkRange.StartX) lStartX--;
	int lEndX = aThreadWorkRange.EndX;
	if (lEndX == aProcessWorkRange.EndX) lEndX++;
	int lStartY = aThreadWorkRange.StartY;
	if (lStartY == aProcessWorkRange.StartY) lStartY--;
	int lEndY = aThreadWorkRange.EndY;
	if (lEndY == aProcessWorkRange.EndY) lEndY++;
	int lStartZ = aThreadWorkRange.StartZ;
	if (lStartZ == aProcessWorkRange.StartZ) lStartZ--;
	int lEndZ = aThreadWorkRange.EndZ;
	if (lEndZ == aProcessWorkRange.EndZ) lEndZ++;

	exprtk::expression<LBM_REAL> lExpression = CreateStandardExpression(aExpressionString);

	lExpression.get_symbol_table(0).get_variable(SizeXString)->ref() = aConfig.GridSizeX - 1;
	lExpression.get_symbol_table(0).get_variable(SizeYString)->ref() = aConfig.GridSizeY - 1;
	lExpression.get_symbol_table(0).get_variable(SizeZString)->ref() = aConfig.GridSizeZ - 1;

	for (int iX = lStartX; iX <= lEndX; iX++)
	{
		for (int iY = lStartY; iY <= lEndY; iY++)
		{
			for (int iZ = lStartZ; iZ <= lEndZ; iZ++)
			{
				int lIndex = GetNodesTypeArrayIndex(iX, iY, iZ, aProcessWorkRange);

				lExpression.get_symbol_table(0).get_variable(XString)->ref() = iX;
				lExpression.get_symbol_table(0).get_variable(YString)->ref() = iY;
				lExpression.get_symbol_table(0).get_variable(ZString)->ref() = iZ;

				LBM_REAL lResult = lExpression.value();
				int lResultInt = static_cast<int>(lResult);

				if (lResultInt == 0)
				{
//					std::cout<<"Setting node "<<iX<<", "<<iY<<", "<<iZ<<" to standard"<<std::endl;
					aNodesType[lIndex] = LatticeNodeTypeEnum::Standard;
				}
				else
				{
//					std::cout<<"Setting node "<<iX<<", "<<iY<<", "<<iZ<<" to solid wall"<<std::endl;
					aNodesType[lIndex] = LatticeNodeTypeEnum::SolidWall;
				}
//				std::cout<<"Expression result = "<<lResult<<std::endl;
			}
		}
	}
}
void StreamLocalFull(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig)
{
	const Direction* const lDirections = LatticeTypeToDirections.at(aConfig.LatticeType);

	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ; iZ <= aThreadWorkRange.EndZ; iZ++)
			{
				StreamLocalOneNode(iX, iY, iZ, lDirections, aSourceWorkArray, aTargetWorkArray, aNodesType, aProcessWorkRange, aThreadWorkRange, aConfig);
			}
		}
	}
}
void StreamLocalInterior(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig)
{
	const Direction* const lDirections = LatticeTypeToDirections.at(aConfig.LatticeType);

	for (int iX = aThreadWorkRange.StartX + 1; iX <= aThreadWorkRange.EndX - 1; iX++)
	{
		for (int iY = aThreadWorkRange.StartY + 1; iY <= aThreadWorkRange.EndY - 1; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ + 1; iZ <= aThreadWorkRange.EndZ - 1; iZ++)
			{
				StreamLocalOneNode(iX, iY, iZ, lDirections, aSourceWorkArray, aTargetWorkArray, aNodesType, aProcessWorkRange, aThreadWorkRange, aConfig);
			}
		}
	}
}
void StreamLocalBorder(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig)
{
	const Direction* const lDirections = LatticeTypeToDirections.at(aConfig.LatticeType);

	// In case the thread has only one node thick layer to deal with, the step from border to border can be zero
	// and threfore the loop would never end
	int lBigStepX = aThreadWorkRange.EndX - aThreadWorkRange.StartX; if (lBigStepX == 0) lBigStepX = 1;
	int lBigStepY = aThreadWorkRange.EndY - aThreadWorkRange.StartY; if (lBigStepY == 0) lBigStepY = 1;
	int lBigStepZ = aThreadWorkRange.EndZ - aThreadWorkRange.StartZ; if (lBigStepZ == 0) lBigStepZ = 1;

	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ; iZ <= aThreadWorkRange.EndZ; iZ += lBigStepZ)
			{
				StreamLocalOneNode(iX, iY, iZ, lDirections, aSourceWorkArray, aTargetWorkArray, aNodesType, aProcessWorkRange, aThreadWorkRange, aConfig);
			}
		}
	}
	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY += lBigStepY)
		{
			for (int iZ = aThreadWorkRange.StartZ + 1; iZ <= aThreadWorkRange.EndZ - 1; iZ++)
			{
				StreamLocalOneNode(iX, iY, iZ, lDirections, aSourceWorkArray, aTargetWorkArray, aNodesType, aProcessWorkRange, aThreadWorkRange, aConfig);
			}
		}
	}
	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX += lBigStepX)
	{
		for (int iY = aThreadWorkRange.StartY + 1; iY <= aThreadWorkRange.EndY - 1; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ + 1; iZ <= aThreadWorkRange.EndZ - 1; iZ++)
			{
				StreamLocalOneNode(iX, iY, iZ, lDirections, aSourceWorkArray, aTargetWorkArray, aNodesType, aProcessWorkRange, aThreadWorkRange, aConfig);
			}
		}
	}
}
void StreamLocalOneNode(const int& aX, const int& aY, const int& aZ, const Direction* const (&aDirections),
		const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig)
{
	int lNodesTypeArrayIndex = GetNodesTypeArrayIndex(aX, aY, aZ, aProcessWorkRange);
	LatticeNodeTypeEnum lNodeType = aNodesType[lNodesTypeArrayIndex];

	if (lNodeType != LatticeNodeTypeEnum::Standard) return;

	for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
	{
		const Direction lDirection = aDirections[iDir];
		int lSourceIndex = GetWorkArrayIndex(iDir, aX, aY, aZ, aProcessWorkRange, aConfig);

		int lNeighbourNodeTypeIndex = GetNodesTypeArrayIndex(
				aX + lDirection.GridDirection.X,
				aY + lDirection.GridDirection.Y,
				aZ + lDirection.GridDirection.Z, aProcessWorkRange);

		LatticeNodeTypeEnum lNeighbourNodeType = aNodesType[lNeighbourNodeTypeIndex];

		int lTargetIndex = -1;
		if (lNeighbourNodeType == LatticeNodeTypeEnum::Standard)
		{
			lTargetIndex = GetWorkArrayIndex(iDir,
					aX + lDirection.GridDirection.X,
					aY + lDirection.GridDirection.Y,
					aZ + lDirection.GridDirection.Z,
					aProcessWorkRange, aConfig);

		}
		else if (lNeighbourNodeType == LatticeNodeTypeEnum::SolidWall)
		{
			lTargetIndex = GetWorkArrayIndex(lDirection.OppositeDirIndex, aX, aY, aZ, aProcessWorkRange, aConfig);
		}

		aTargetWorkArray[lTargetIndex] = aSourceWorkArray[lSourceIndex];
	}
}


void Collision(LBM_REAL* const (&aWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange, const ConfigStruct& aConfig)
{
	const Direction* const lDirections = LatticeTypeToDirections.at(aConfig.LatticeType);

	for (int iX = aThreadWorkRange.StartX; iX <= aThreadWorkRange.EndX; iX++)
	{
		for (int iY = aThreadWorkRange.StartY; iY <= aThreadWorkRange.EndY; iY++)
		{
			for (int iZ = aThreadWorkRange.StartZ; iZ <= aThreadWorkRange.EndZ; iZ++)
			{
				int lNodesTypeArrayIndex = GetNodesTypeArrayIndex(iX, iY, iZ, aProcessWorkRange);
				LatticeNodeTypeEnum lNodeType = aNodesType[lNodesTypeArrayIndex];

				if (lNodeType != LatticeNodeTypeEnum::Standard) continue;

				LBM_REAL lXVelocity = 0;
				LBM_REAL lYVelocity = 0;
				LBM_REAL lZVelocity = 0;
				LBM_REAL lDensity = 0;

				for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
				{
					int lIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);
					const Direction* const lDir = lDirections + iDir;

					lXVelocity += lDir->X * aWorkArray[lIndex];
					lYVelocity += lDir->Y * aWorkArray[lIndex];
					lZVelocity += lDir->Z * aWorkArray[lIndex];

					lDensity += aWorkArray[lIndex];
				}

				lXVelocity /= lDensity;
				lYVelocity /= lDensity;
				lZVelocity /= lDensity;

				LBM_REAL lVelDotVel = lXVelocity * lXVelocity + lYVelocity * lYVelocity + lZVelocity * lZVelocity;

				for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
				{
					int lIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);
					const Direction* const lDir = lDirections + iDir;

					LBM_REAL lVelDotDir = lXVelocity * lDir->X + lYVelocity * lDir->Y + lZVelocity * lDir->Z;

					// Lattice velocity (in LB units) is considered 1
					LBM_REAL lFEq = lDensity * lDir->Weight *
							(
									1 +
									3.0 * lVelDotDir +
									9.0/2 * lVelDotDir * lVelDotDir +
									-3.0/2 * lVelDotVel
							);
					LBM_REAL lCurrentValue = aWorkArray[lIndex];
					aWorkArray[lIndex] = lCurrentValue + 1 / aConfig.RelaxationParameter * (lFEq - lCurrentValue);
				}
			}
		}
	}
}
void ApplyBoundaryConditions(LBM_REAL* const (&aWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const std::vector<BCStruct>& aBCForThread,
		const ConfigStruct& aConfig)
{
	LBM_REAL lDeltaCoeff1 = LatticeTypeToDeltaCoeff1.at(aConfig.LatticeType);
	LBM_REAL lDeltaCoeff2 = LatticeTypeToDeltaCoeff2.at(aConfig.LatticeType);

	for (int iBC = 0; iBC < aBCForThread.size(); iBC++)
	{
		BCStruct lBC = aBCForThread.at(iBC);
		RangeStruct lBCRange = lBC.Core.Range;
		exprtk::expression<LBM_REAL> lExpression1 = lBC.Expression1;
		exprtk::expression<LBM_REAL> lExpression2 = lBC.Expression2;
		exprtk::expression<LBM_REAL> lExpression3 = lBC.Expression3;

		SetExpressionSizeParameters(lExpression1, aConfig);
		SetExpressionSizeParameters(lExpression2, aConfig);
		SetExpressionSizeParameters(lExpression3, aConfig);

		GridDirectionStruct lTangentA;
		GridDirectionStruct lTangentB;

		BaseDirection lUp = BaseDirection::Up;
		if 		(lBC.Core.OuterNormal.X != 0) { lTangentA.Y = lUp; lTangentB.Z = lUp; }
		else if (lBC.Core.OuterNormal.Y != 0) { lTangentA.X = lUp; lTangentB.Z = lUp; }
		else if (lBC.Core.OuterNormal.Z != 0) { lTangentA.X = lUp; lTangentB.Y = lUp; }
		else { std::cout<<"Invalid boundary condition normal vector!"<<std::endl; exit(1); }

		for (int iX = lBCRange.StartX; iX <= lBCRange.EndX; iX++)
		{
			for (int iY = lBCRange.StartY; iY <= lBCRange.EndY; iY++)
			{
				for (int iZ = lBCRange.StartZ; iZ <= lBCRange.EndZ; iZ++)
				{
					int lNodeIndex = GetNodesTypeArrayIndex(iX, iY, iZ, aProcessWorkRange);
					LatticeNodeTypeEnum lNodeType = aNodesType[lNodeIndex];
					if (lNodeType == LatticeNodeTypeEnum::SolidWall) continue;

					SetExpressionPositionParameters(lExpression1, iX, iY, iZ);

					LBM_REAL lSum = 0;
					for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
					{
						const Direction* const lDir = LatticeTypeToDirections.at(aConfig.LatticeType) + iDir;
						int lNormalDotDir =
								lBC.Core.OuterNormal.X * lDir->GridDirection.X +
								lBC.Core.OuterNormal.Y * lDir->GridDirection.Y +
								lBC.Core.OuterNormal.Z * lDir->GridDirection.Z;

						if (lNormalDotDir < 0) continue;
						int lWorkArrayIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);

						if (lNormalDotDir == 0) lSum += aWorkArray[lWorkArrayIndex];
						else lSum += 2 * aWorkArray[lWorkArrayIndex];
					}

					// Calculate dnesity and normal velocity for the node
					LBM_REAL lDensity;
					LBM_REAL lNormalVelocity;
					LBM_REAL lTangentAVelocity = lExpression2.value();
					LBM_REAL lTangentBVelocity = lExpression3.value();

					if (lBC.Core.Type == BCTypeEnum::Density)
					{
						lDensity = lExpression1.value();
						lNormalVelocity = lSum / lDensity - 1;
					}
					else if (lBC.Core.Type == BCTypeEnum::NormalVelocity)
					{
						lNormalVelocity = lExpression1.value();
						lDensity = lSum / (lNormalVelocity + 1);
					}
					else if (lBC.Core.Type == BCTypeEnum::NormalDensityGradient)
					{
						// Calculate the density at the two previous nodes (in outer normal direction)
						LBM_REAL lPrevNodeDensity = 0;
						LBM_REAL lPrevPrevNodeDensity = 0;

						for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
						{
							int lPrevNodeWorkArrayIndex = GetWorkArrayIndex(iDir,
									iX - lBC.Core.OuterNormal.X, iY - lBC.Core.OuterNormal.Y, iZ - lBC.Core.OuterNormal.Z,
									aProcessWorkRange, aConfig);

							int lPrevPrevNodeWorkArrayIndex = GetWorkArrayIndex(iDir,
									iX - 2 * lBC.Core.OuterNormal.X, iY - 2 * lBC.Core.OuterNormal.Y, iZ - 2 * lBC.Core.OuterNormal.Z,
									aProcessWorkRange, aConfig);

							lPrevNodeDensity += aWorkArray[lPrevNodeWorkArrayIndex];
							lPrevPrevNodeDensity += aWorkArray[lPrevPrevNodeWorkArrayIndex];
						}

						// Get the Neumann bc
						LBM_REAL lOuterNormalDerivative = lExpression1.value();

						// Transform the Neumann bc to the Dirichlet bc
						lDensity =
								(4 * lPrevNodeDensity - lPrevPrevNodeDensity
								+ 2 * lOuterNormalDerivative) / 3;
						//lDensity = lPrevNodeDensity;

						lNormalVelocity = lSum / lDensity - 1;
					}
					else
					{
						std::cout<<"Unknown boundary condition type! ("<<lBC.Core.Type<<")"<<std::endl;
						exit(1);
					}

					// Calculate deltas to achieve correct tangent velocities
					LBM_REAL lDeltaA = 0;
					LBM_REAL lDeltaB = 0;

					for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
					{
						const Direction* const lDir = LatticeTypeToDirections.at(aConfig.LatticeType) + iDir;
						int lNormalDotDir =
								lBC.Core.OuterNormal.X * lDir->GridDirection.X +
								lBC.Core.OuterNormal.Y * lDir->GridDirection.Y +
								lBC.Core.OuterNormal.Z * lDir->GridDirection.Z;
						if (lNormalDotDir != 0) continue;
						int lWorkArrayIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);

						lDeltaA += -lDeltaCoeff1 * (lTangentA.X * lDir->GridDirection.X + lTangentA.Y * lDir->GridDirection.Y + lTangentA.Z * lDir->GridDirection.Z)
								* aWorkArray[lWorkArrayIndex];

						lDeltaB += -lDeltaCoeff1 * (lTangentB.X * lDir->GridDirection.X + lTangentB.Y * lDir->GridDirection.Y + lTangentB.Z * lDir->GridDirection.Z)
								* aWorkArray[lWorkArrayIndex];
					}

					lDeltaA += lDeltaCoeff2 * lDensity * lTangentAVelocity;
					lDeltaB += lDeltaCoeff2 * lDensity * lTangentBVelocity;

					// Calculate unknown distribution values
					for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
					{
						const Direction* const lDir = LatticeTypeToDirections.at(aConfig.LatticeType) + iDir;
						int lNormalDotDir = lBC.Core.OuterNormal.X * lDir->GridDirection.X + lBC.Core.OuterNormal.Y * lDir->GridDirection.Y + lBC.Core.OuterNormal.Z * lDir->GridDirection.Z;
						if (lNormalDotDir >= 0) continue;
						int lOppDirIndex = lDir->OppositeDirIndex;
						int lWorkArrayIndex = GetWorkArrayIndex(iDir, iX, iY, iZ, aProcessWorkRange, aConfig);
						int lWorkArrayOppIndex = GetWorkArrayIndex(lOppDirIndex, iX, iY, iZ, aProcessWorkRange, aConfig);
						LBM_REAL lOppValue = aWorkArray[lWorkArrayOppIndex];

						int lTangentADotDir = lDir->GridDirection.X * lTangentA.X + lDir->GridDirection.Y * lTangentA.Y + lDir->GridDirection.Z * lTangentA.Z;
						int lTangentBDotDir = lDir->GridDirection.X * lTangentB.X + lDir->GridDirection.Y * lTangentB.Y + lDir->GridDirection.Z * lTangentB.Z;

						aWorkArray[lWorkArrayIndex] = lOppValue +
								6 * lDensity * lDir->Weight * (-lNormalVelocity + lTangentAVelocity * lTangentADotDir + lTangentBVelocity  * lTangentBDotDir)
								+ lDeltaA * lTangentADotDir
								+ lDeltaB * lTangentBDotDir;
					}
				}
			}
		}
	}
}
