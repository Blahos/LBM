#pragma once
#include <iostream>
#include <string>
#include <unistd.h>
#include <cstring>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstddef>
#include <map>
#include <cassert>
#include <chrono>

#include <omp.h>
#include <mpi.h>

#include <vtk-7.1/vtkImageData.h>
#include <vtk-7.1/vtkVersion.h>
#include <vtk-7.1/vtkSmartPointer.h>
#include <vtk-7.1/vtkXMLImageDataWriter.h>
#include <vtk-7.1/vtkPointData.h>
#include <vtk-7.1/vtkDoubleArray.h>

#include "exprtk.hpp"
