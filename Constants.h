#pragma once
#include <string>
#define PI 3.1415926535
#define SizeXString std::string("SizeX")
#define SizeYString std::string("SizeY")
#define SizeZString std::string("SizeZ")
#define XString std::string("x")
#define YString std::string("y")
#define ZString std::string("z")
#define MainConfigName std::string("MainConfig.cfg")
