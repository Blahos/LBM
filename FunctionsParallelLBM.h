#pragma once
#include "Includes.h"
#include "Types.h"
#include "LatticeNodeTypeEnum.h"
#include "RangeStruct.h"
#include "CommSettingsStruct.h"
#include "ConfigStruct.h"
#include "Functions.h"
#include "ConstantsLBM.h"

// Run only once per mpi process, i.e. only in one OpenMP thread
void StreamComm(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange,
		const ConfigStruct& aConfig, const CommSettingsStruct& aCommSettings);
