struct Direction;

#pragma once
#include "Types.h"
#include "GridDirectionStruct.h"

struct Direction
{
public:
	GridDirectionStruct GridDirection;

	LBM_REAL X;
	LBM_REAL Y;
	LBM_REAL Z;

	LBM_REAL Weight;

	int OppositeDirIndex;
};
