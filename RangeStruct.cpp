
#include "RangeStruct.h"


RangeStruct RangeStruct::Shift(const GridDirectionStruct& aDir)
{
	RangeStruct lReturnRange(*this);

	lReturnRange.StartX += aDir.X;
	lReturnRange.EndX += aDir.X;
	lReturnRange.StartY += aDir.Y;
	lReturnRange.EndY += aDir.Y;
	lReturnRange.StartZ += aDir.Z;
	lReturnRange.EndZ += aDir.Z;

	return lReturnRange;
}
