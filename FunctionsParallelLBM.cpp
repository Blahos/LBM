#include "FunctionsParallelLBM.h"


void StreamComm(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange,
		const ConfigStruct& aConfig, const CommSettingsStruct& aCommSettings)
{
	int lPafito = 0;
	int lMessagesToSendCount = 0;
	int lMessagesToReceiveCount = 0;

	for (auto nIterator = aCommSettings.MessagesToSend.begin(); nIterator != aCommSettings.MessagesToSend.end(); nIterator++)
	{
		lMessagesToSendCount += nIterator->second.size();
	}
	for (auto nIterator = aCommSettings.MessagesToReceive.begin(); nIterator != aCommSettings.MessagesToReceive.end(); nIterator++)
	{
		lMessagesToReceiveCount += nIterator->second.size();
	}

	LBM_REAL** lMessagesToSendData = new LBM_REAL*[lMessagesToSendCount];
	LBM_REAL** lMessagesToReceiveData = new LBM_REAL*[lMessagesToReceiveCount];

	MPI_Request* lSendRequests = new MPI_Request[lMessagesToSendCount];

	int lMessageToSendIndex = 0;
	// Non-blocking send
	for (auto nIterator = aCommSettings.MessagesToSend.begin(); nIterator != aCommSettings.MessagesToSend.end(); nIterator++)
	{
		int lTargetProcessId = nIterator->first;
		for (int iMessage = 0; iMessage < nIterator->second.size(); iMessage++)
		{
			int lDirIndex = nIterator->second[iMessage].DirectionIndex;
			const RangeStruct* const lMessageDataRange = &(nIterator->second[iMessage].DataRange);
			int lMessageSize = nIterator->second[iMessage].DataSize;

			lMessagesToSendData[lMessageToSendIndex] = new LBM_REAL[lMessageSize];

			for (int iX = lMessageDataRange->StartX; iX <= lMessageDataRange->EndX; iX++)
			{
				for (int iY = lMessageDataRange->StartY; iY <= lMessageDataRange->EndY; iY++)
				{
					for (int iZ = lMessageDataRange->StartZ; iZ <= lMessageDataRange->EndZ; iZ++)
					{
						int lWorkArrayIndex = GetWorkArrayIndex(lDirIndex, iX, iY, iZ, aProcessWorkRange, aConfig);
						// GetMacroArrayIndex is not exactly what is happening here, but it works, because there is only one direction stored
						// in the send data, so it effectively is like macro array. And also, this function doesn't consider any array extension,
						// which is what we want here.
						int lMessageToSendDataIndex = GetMacroArrayIndex(iX, iY, iZ, *lMessageDataRange);
						LBM_REAL lTemp = aSourceWorkArray[lWorkArrayIndex];
						lMessagesToSendData[lMessageToSendIndex][lMessageToSendDataIndex] = lTemp;
					}
				}
			}

			MPI_Isend(lMessagesToSendData[lMessageToSendIndex], lMessageSize, LBM_REAL_MPI, lTargetProcessId, lDirIndex, MPI_COMM_WORLD,
					lSendRequests + lMessageToSendIndex);

			lMessageToSendIndex++;
		}
	}

	int lMessageToReceiveIndex = 0;
	// Receive
	for (auto nIterator = aCommSettings.MessagesToReceive.begin(); nIterator != aCommSettings.MessagesToReceive.end(); nIterator++)
	{
		int lSourceProcessId = nIterator->first;
		for (int iMessage = 0; iMessage < nIterator->second.size(); iMessage++)
		{
			int lDirIndex = nIterator->second[iMessage].DirectionIndex;
			const RangeStruct* const lMessageDataRange = &(nIterator->second[iMessage].DataRange);
			int lMessageSize = nIterator->second[iMessage].DataSize;

			lMessagesToReceiveData[lMessageToReceiveIndex] = new LBM_REAL[lMessageSize];

			MPI_Recv(lMessagesToReceiveData[lMessageToReceiveIndex], lMessageSize, LBM_REAL_MPI, lSourceProcessId, lDirIndex, MPI_COMM_WORLD,
					MPI_STATUS_IGNORE);

			lMessageToReceiveIndex++;
		}
	}

	// Wait for all sends to finish
	for (int iMessage = 0; iMessage < lMessagesToSendCount; iMessage++)
	{
		MPI_Wait(lSendRequests + iMessage, MPI_STATUS_IGNORE);
	}

	int lReceivedMessageIndex = 0;
	// Write received values to work array
	for (auto nIterator = aCommSettings.MessagesToReceive.begin(); nIterator != aCommSettings.MessagesToReceive.end(); nIterator++)
	{
		int lSourceProcessId = nIterator->first;
		for (int iMessage = 0; iMessage < nIterator->second.size(); iMessage++)
		{
			int lDirIndex  = nIterator->second[iMessage].DirectionIndex;
			const Direction* const lDirection = &(LatticeTypeToDirections.at(aConfig.LatticeType)[lDirIndex]);
			const RangeStruct* const lMessageDataRange = &(nIterator->second[iMessage].DataRange);

			for (int iX = lMessageDataRange->StartX; iX <= lMessageDataRange->EndX; iX++)
			{
				for (int iY = lMessageDataRange->StartY; iY <= lMessageDataRange->EndY; iY++)
				{
					for (int iZ = lMessageDataRange->StartZ; iZ <= lMessageDataRange->EndZ; iZ++)
					{
						int lSourceNodeTypeArrayIndex = GetNodesTypeArrayIndex(
								iX - lDirection->GridDirection.X,
								iY - lDirection->GridDirection.Y,
								iZ - lDirection->GridDirection.Z, aProcessWorkRange);
						// If the value is coming from the solid wall, ignore it.
						if (aNodesType[lSourceNodeTypeArrayIndex] == LatticeNodeTypeEnum::SolidWall) continue;

						int lTargetWorkArrayIndex = GetWorkArrayIndex(lDirIndex,
								iX, iY, iZ,
								aProcessWorkRange, aConfig);
						// GetMacroArrayIndex is not exactly what is happening here, but it works, because there is only one direction stored
						// in the send data, so it effectively is like macro array. And also, this function doesn't consider any array extension,
						// which is what we want here.
						int lReceivedMessageDataIndex = GetMacroArrayIndex(iX, iY, iZ, *lMessageDataRange);
						aTargetWorkArray[lTargetWorkArrayIndex] = lMessagesToReceiveData[lReceivedMessageIndex][lReceivedMessageDataIndex];
					}
				}
			}

			lReceivedMessageIndex++;
		}
	}

	// Free memory
	delete[] lSendRequests;
	for (int iMessage = 0; iMessage < lMessagesToSendCount; iMessage++)
	{
		delete[] lMessagesToSendData[iMessage];
	}
	delete[] lMessagesToSendData;
	for (int iMessage = 0; iMessage < lMessagesToReceiveCount; iMessage++)
	{
		delete[] lMessagesToReceiveData[iMessage];
	}
	delete[] lMessagesToReceiveData;
}

