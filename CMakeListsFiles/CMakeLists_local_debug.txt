cmake_minimum_required(VERSION 3.0.0)
project(LBM VERSION 0.0.0)

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

set(VTK_DIR "/home/jirka/Libraries/VTK/build")
find_package(MPI REQUIRED)
include_directories(${MPI_INCLUDE_PATH})

include(CTest)
enable_testing()
FIND_PACKAGE( OpenMP)
add_executable(LBM main.cpp RangeStruct.cpp FunctionsCommon.cpp Functions.cpp FunctionsParallel.cpp FunctionsLBM.cpp FunctionsParallelLBM.cpp)

SET(GCC_COVERAGE_COMPILE_FLAGS "-g -Og")
add_definitions(${GCC_COVERAGE_COMPILE_FLAGS})

target_link_libraries(LBM ${MPI_LIBRARIES})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)


if(MPI_COMPILE_FLAGS)
  set_target_properties(LBM PROPERTIES
    COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
  set_target_properties(LBM PROPERTIES
    LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
target_link_libraries(LBM ${VTK_LIBRARIES})

set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")

target_compile_features(LBM PRIVATE cxx_range_for)
