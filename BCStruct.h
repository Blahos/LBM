struct BCStruct;

#pragma once
#include "Includes.h"
#include "BCStructCore.h"
#include "RangeStruct.h"
#include "GridDirectionStruct.h"

struct BCStruct
{
public:
	BCStructCore Core;

	// Expressions corresponding to strings ExpressionString1, ExpressionString2 and ExpressionString3 in Core
	exprtk::expression<LBM_REAL> Expression1;
	exprtk::expression<LBM_REAL> Expression2;
	exprtk::expression<LBM_REAL> Expression3;
};
