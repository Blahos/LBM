
#include "Functions.h"

std::string LoadExpressionString(const std::string& aFilePath)
{
	std::vector<std::string> lExpressionStringVector = LoadExpressionStrings(aFilePath, 1);
	return lExpressionStringVector[0];
}
std::vector<std::string> LoadExpressionStrings(const std::string& aFilePath, int aCount)
{
    std::fstream lFile(aFilePath.c_str(), std::ios_base::in);

    std::vector<std::string> lExpressionStrings;

    if (!lFile)
    {
        std::cout<<"Failed to open file "<<aFilePath<<"!"<<std::endl;
        exit(0);
    }
    for (int i = 0; i < aCount; i++)
    {
    	std::string lString;
    	getline(lFile, lString);
//    	lFile >> lString;
    	lExpressionStrings.push_back(lString);
    }
    lFile.close();

    return lExpressionStrings;
}
exprtk::expression<LBM_REAL> CreateStandardExpression(const std::string& aExpressionString)
{
    exprtk::symbol_table<LBM_REAL> lSymbolTable;
    lSymbolTable.create_variable(SizeXString);
    lSymbolTable.create_variable(SizeYString);
    lSymbolTable.create_variable(SizeZString);

    lSymbolTable.create_variable(XString);
    lSymbolTable.create_variable(YString);
    lSymbolTable.create_variable(ZString);

    exprtk::expression<LBM_REAL> lExpression;
    lExpression.register_symbol_table(lSymbolTable);

    exprtk::parser<LBM_REAL> lParser;
    lParser.compile(aExpressionString, lExpression);

    return lExpression;
}
std::vector<BCStruct> CreateBoundaryConditions(const RangeStruct& aThreadWorkRange, const std::vector<BCStructCore>& aBCsForProcess)
{
	int lCount = aBCsForProcess.size();
    std::vector<BCStruct> lConditions;

    for (int i = 0; i < lCount; i++)
    {
    	BCStructCore lProcBC = aBCsForProcess[i];
    	BCStruct lBC;

    	lBC.Core.Range = lProcBC.Range;
    	if (lBC.Core.Range.StartX < aThreadWorkRange.StartX) lBC.Core.Range.StartX = aThreadWorkRange.StartX;
    	if (lBC.Core.Range.EndX > aThreadWorkRange.EndX) lBC.Core.Range.EndX = aThreadWorkRange.EndX;
    	if (lBC.Core.Range.StartY < aThreadWorkRange.StartY) lBC.Core.Range.StartY = aThreadWorkRange.StartY;
    	if (lBC.Core.Range.EndY > aThreadWorkRange.EndY) lBC.Core.Range.EndY = aThreadWorkRange.EndY;
    	if (lBC.Core.Range.StartZ < aThreadWorkRange.StartZ) lBC.Core.Range.StartZ = aThreadWorkRange.StartZ;
    	if (lBC.Core.Range.EndZ > aThreadWorkRange.EndZ) lBC.Core.Range.EndZ = aThreadWorkRange.EndZ;

    	if (lBC.Core.Range.StartX > lBC.Core.Range.EndX || lBC.Core.Range.StartY > lBC.Core.Range.EndY || lBC.Core.Range.StartZ > lBC.Core.Range.EndZ)
    	{
    		// Discard the condition, since this thread wont be dealing with it
    		continue;
    	}

    	lBC.Core.ExpressionString1 = lProcBC.ExpressionString1;
    	lBC.Core.ExpressionString2 = lProcBC.ExpressionString2;
    	lBC.Core.ExpressionString3 = lProcBC.ExpressionString3;

    	lBC.Expression1 = CreateStandardExpression(lBC.Core.ExpressionString1);
    	lBC.Expression2 = CreateStandardExpression(lBC.Core.ExpressionString2);
    	lBC.Expression3 = CreateStandardExpression(lBC.Core.ExpressionString3);

    	lBC.Core.OuterNormal = lProcBC.OuterNormal;
    	lBC.Core.Type = lProcBC.Type;

    	lConditions.push_back(lBC);
    }

    return lConditions;
}
void SetExpressionSizeParameters(exprtk::expression<LBM_REAL>& aExpression, const ConfigStruct& aConfig)
{
	aExpression.get_symbol_table(0).get_variable(SizeXString)->ref() = aConfig.GridSizeX - 1;
	aExpression.get_symbol_table(0).get_variable(SizeYString)->ref() = aConfig.GridSizeY - 1;
	aExpression.get_symbol_table(0).get_variable(SizeZString)->ref() = aConfig.GridSizeZ - 1;
}
void SetExpressionPositionParameters(exprtk::expression<LBM_REAL>& aExpression, int aX, int aY, int aZ)
{
	aExpression.get_symbol_table(0).get_variable(XString)->ref() = aX;
	aExpression.get_symbol_table(0).get_variable(YString)->ref() = aY;
	aExpression.get_symbol_table(0).get_variable(ZString)->ref() = aZ;
}
int GetWorkArraySize(const RangeStruct& aProcessWorkRangeStruct, const ConfigStruct& aConfig)
{
	// There is an extra layer of the grid on the each side of the working area, thus + 2 in each dimension
	int lArraySize =
			(aProcessWorkRangeStruct.EndX - aProcessWorkRangeStruct.StartX + 1 + 2) *
			(aProcessWorkRangeStruct.EndY - aProcessWorkRangeStruct.StartY + 1 + 2) *
			(aProcessWorkRangeStruct.EndZ - aProcessWorkRangeStruct.StartZ + 1 + 2) *
			aConfig.LatticeType;

	return lArraySize;
}
int GetMacroArraySize(const RangeStruct& aProcessWorkRangeStruct)
{
	int lArraySize =
			(aProcessWorkRangeStruct.EndX - aProcessWorkRangeStruct.StartX + 1) *
			(aProcessWorkRangeStruct.EndY - aProcessWorkRangeStruct.StartY + 1) *
			(aProcessWorkRangeStruct.EndZ - aProcessWorkRangeStruct.StartZ + 1);

	return lArraySize;
}
int GetNodesTypeArraySize(const RangeStruct& aProcessWorkRangeStruct)
{
	// Nodes type array is macro, but its extended
	int lArraySize =
			(aProcessWorkRangeStruct.EndX - aProcessWorkRangeStruct.StartX + 1 + 2) *
			(aProcessWorkRangeStruct.EndY - aProcessWorkRangeStruct.StartY + 1 + 2) *
			(aProcessWorkRangeStruct.EndZ - aProcessWorkRangeStruct.StartZ + 1 + 2);

	return lArraySize;
}
void InitialiseWorkArray(LBM_REAL* (&aArray), const RangeStruct& aProcessWorkRange, const ConfigStruct& aConfig)
{
	int lArraySize = GetWorkArraySize(aProcessWorkRange, aConfig);
	aArray = new LBM_REAL[lArraySize];
}
void InitialiseMacroArray(LBM_REAL* (&aArray), const RangeStruct& aProcessWorkRange)
{
	int lArraySize = GetMacroArraySize(aProcessWorkRange);
	aArray = new LBM_REAL[lArraySize];
}
void InitialiseNodesTypeArray(LatticeNodeTypeEnum* (&aArray), const RangeStruct& aProcessWorkRange)
{
	int lArraySize = GetNodesTypeArraySize(aProcessWorkRange);
	aArray = new LatticeNodeTypeEnum[lArraySize];
}

int GetWorkArrayIndex(const int& aDirIndex, const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange, const ConfigStruct& aConfig)
{
	int lDirCount = aConfig.LatticeType;

	int lXRel = aX - aProcessWorkRange.StartX + 1;
	int lYRel = aY - aProcessWorkRange.StartY + 1;
	int lZRel = aZ - aProcessWorkRange.StartZ + 1;

	// Work array is extended by one layer on each side, thus + 2
	int lSizeX = (aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1 + 2);
	int lSizeY = (aProcessWorkRange.EndY - aProcessWorkRange.StartY + 1 + 2);

	int lNodeIndex =
			lXRel +
			lYRel * lSizeX +
			lZRel * lSizeX * lSizeY;

	int lIndex = lNodeIndex * lDirCount + aDirIndex;
	return lIndex;
}
int GetMacroArrayIndex(const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange)
{
	int lXRel = aX - aProcessWorkRange.StartX;
	int lYRel = aY - aProcessWorkRange.StartY;
	int lZRel = aZ - aProcessWorkRange.StartZ;

	int lSizeX = (aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1);
	int lSizeY = (aProcessWorkRange.EndY - aProcessWorkRange.StartY + 1);

	int lIndex =
			lXRel +
			lYRel * lSizeX +
			lZRel * lSizeX * lSizeY;

	return lIndex;
}
int GetNodesTypeArrayIndex(const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange)
{
	int lXRel = aX - aProcessWorkRange.StartX + 1;
	int lYRel = aY - aProcessWorkRange.StartY + 1;
	int lZRel = aZ - aProcessWorkRange.StartZ + 1;

	int lSizeX = (aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1 + 2);
	int lSizeY = (aProcessWorkRange.EndY - aProcessWorkRange.StartY + 1 + 2);

	int lIndex =
			lXRel +
			lYRel * lSizeX +
			lZRel * lSizeX * lSizeY;

	return lIndex;
}

void WriteMacroArrays(const LBM_REAL* const aDensity, const LBM_REAL* const aVelocityX, const LBM_REAL* const aVelocityY, const LBM_REAL* const aVelocityZ, const RangeStruct& aProcessWorkRange, const std::string aFileName, const ConfigStruct& aConfig)
{
	int lSizeX = aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1;
	int lSizeY = aProcessWorkRange.EndY - aProcessWorkRange.StartY + 1;
	int lSizeZ = aProcessWorkRange.EndZ - aProcessWorkRange.StartZ + 1;

	vtkSmartPointer<vtkImageData> lImageData = vtkSmartPointer<vtkImageData>::New();
	lImageData->SetDimensions(lSizeX, lSizeY, lSizeZ);
//	lImageData->AllocateScalars(LBM_REAL_VTK, 0);

	vtkSmartPointer<LBM_VTK_ARRAY> lDensityArray = vtkSmartPointer<LBM_VTK_ARRAY>::New();
	vtkSmartPointer<LBM_VTK_ARRAY> lVelocityArray = vtkSmartPointer<LBM_VTK_ARRAY>::New();

	lDensityArray->SetNumberOfComponents(1);
	lDensityArray->SetNumberOfTuples(lSizeX * lSizeY * lSizeZ);
	lDensityArray->SetComponentName(vtkIdType(0), "Density");
	lDensityArray->SetName("Density");

	lVelocityArray->SetNumberOfComponents(3);
	lVelocityArray->SetNumberOfTuples((lSizeX * lSizeY * lSizeZ));
	lVelocityArray->SetComponentName(vtkIdType(0), "Velocity X");
	lVelocityArray->SetComponentName(vtkIdType(1), "Velocity Y");
	lVelocityArray->SetComponentName(vtkIdType(2), "Velocity Z");
	lVelocityArray->SetName("Velocity");

	lImageData->GetPointData()->AddArray(lDensityArray);
	lImageData->GetPointData()->AddArray(lVelocityArray);

	//lImageData->SetOrigin(aProcessWorkRange.StartX, aProcessWorkRange.StartY, aProcessWorkRange.StartZ);

	for (int iX = aProcessWorkRange.StartX; iX <= aProcessWorkRange.EndX; iX++)
	{
		for (int iY = aProcessWorkRange.StartY; iY <= aProcessWorkRange.EndY; iY++)
		{
			for (int iZ = aProcessWorkRange.StartZ; iZ <= aProcessWorkRange.EndZ; iZ++)
			{
				int lXRel = iX - aProcessWorkRange.StartX;
				int lYRel = iY - aProcessWorkRange.StartY;
				int lZRel = iZ - aProcessWorkRange.StartZ;

				int lIndex = GetMacroArrayIndex(iX, iY, iZ, aProcessWorkRange);
				LBM_REAL lDensity = aDensity[lIndex];
				LBM_REAL lVelocityX = aVelocityX[lIndex] * aConfig.VelocityCoeff;
				LBM_REAL lVelocityY = aVelocityY[lIndex] * aConfig.VelocityCoeff;
				LBM_REAL lVelocityZ = aVelocityZ[lIndex] * aConfig.VelocityCoeff;

//				if (lVelocityX < 0.01) std::cout<<"Setting X velocity "<<lVelocityX<<" to [X, Y, Z] = ["<<iX<<", "<<iY<<", "<<iZ<<"]"<<std::endl;

				vtkIdType lId = lImageData->FindPoint(lXRel, lYRel, lZRel);
				lDensityArray->SetTuple1(lId, lDensity);
				lVelocityArray->SetTuple3(lId, lVelocityX, lVelocityY, lVelocityZ);
			}
		}
	}


	vtkSmartPointer<vtkXMLImageDataWriter> lWriter = vtkSmartPointer<vtkXMLImageDataWriter>::New();
	lWriter->SetFileName(aFileName.c_str());

#if VTK_MAJOR_VERSION <= 5
	lWriter->SetInputConnection(lImageData->GetProducerPort());
#else
	lWriter->SetInputData(lImageData);
#endif

	lWriter->Write();

	if (aConfig.IsVerbose)
		std::cout<<"Density and velocities were successfully written into the file \""<<aFileName<<"\""<<std::endl;
}
std::string GetFileName(const int& aIterationNumber, const int& aProcessNumber)
{
	return std::string("Output/VTK/Output_" + std::to_string(aProcessNumber) + "_" + std::to_string(aIterationNumber) + ".vti");
}
void PrintWorkRange(const RangeStruct& aRange)
{
	std::cout<<"X: ["<<aRange.StartX<<", "<<aRange.EndX<<"]"<<std::endl;
	std::cout<<"Y: ["<<aRange.StartY<<", "<<aRange.EndY<<"]"<<std::endl;
	std::cout<<"Z: ["<<aRange.StartZ<<", "<<aRange.EndZ<<"]"<<std::endl;
}
void PrintDirection(const Direction& aDirection)
{
	std::cout<<"Direction - grid coordinates: ["<<aDirection.GridDirection.X<<", "<<aDirection.GridDirection.Y<<", "<<aDirection.GridDirection.Z<<"]"<<std::endl;
	std::cout<<"Direction - real coordinates: ["<<aDirection.X<<", "<<aDirection.Y<<", "<<aDirection.Z<<"]"<<std::endl;
	std::cout<<"Direction - weight: ["<<aDirection.Weight<<"]"<<std::endl;
	std::cout<<"Direction - opposite direction index: "<<aDirection.OppositeDirIndex<<std::endl;
}
void PrintStreamingMessage(const StreamingMessageStruct& aMessage)
{
	std::cout<<"Streaming message - direction index: "<<aMessage.DirectionIndex<<std::endl;
	std::cout<<"Streaming message - data range: "<<std::endl;
	PrintWorkRange(aMessage.DataRange);
}
void PrintConfig(int aProcessCount, int aThreadCount, const ConfigStruct& aConfig)
{
    int lPad = 45;
	std::cout<<std::setw(lPad)<<std::left<<"MPI processes: "<<aProcessCount<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"OpenMP threads (per process): "<<aThreadCount<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Lattice type: "<<LatticeTypeToLatticeName.at(aConfig.LatticeType)<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Lattice size: "<<aConfig.GridSizeX<<" x "<<aConfig.GridSizeY<<" x "<<aConfig.GridSizeZ<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Time step count: "<<aConfig.TimeStepCount<<std::endl;
	if (aConfig.FileOutputStepPeriod > 0)
		std::cout<<std::setw(lPad)<<std::left<<"File output period: "<<aConfig.FileOutputStepPeriod<<std::endl;
	else
		std::cout<<std::setw(lPad)<<std::left<<"File output period: "<<"disabled"<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Relaxation parameter: "<<aConfig.RelaxationParameter<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Initial condition cfg file path: "<<aConfig.InitialConditionFilePath<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Boundary conditions cfg file path: "<<aConfig.BoundaryCondFilePath<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Wall cfg file path: "<<aConfig.WallSpecFilePath<<std::endl;
	std::cout<<std::setw(lPad)<<std::left<<"Is verbose: "<<aConfig.IsVerbose<<std::endl;
	if (!aConfig.IsVerbose) std::cout<<std::setw(lPad)<<std::left<<"Console output period: "<<aConfig.StepOutputPeriod<<std::endl;
}
void PrintMessage(const std::string& aMessage, int aProcessId, int aThreadId, bool aVerboseOnly, const ConfigStruct& aConfig)
{
	if (aVerboseOnly && !aConfig.IsVerbose) return;
	std::stringstream lStream;

	std::string lThreadIdString;
	if (aThreadId >= 0) lThreadIdString = std::to_string(aThreadId);
	else lThreadIdString = "-";
	lStream<<"Process "<<aProcessId<<", thread "<<lThreadIdString<<": "<<aMessage<<std::endl;

	std::cout<<lStream.str();
}
