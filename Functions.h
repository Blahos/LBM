
#pragma once
#include "CommSettingsStruct.h"
#include "ConfigStruct.h"
#include "Includes.h"
#include "Types.h"
#include "LatticeNodeTypeEnum.h"
#include "DirectionStruct.h"
#include "Constants.h"
#include "BCStruct.h"

/// Config management functions
std::string LoadExpressionString(const std::string& aFilePath);
std::vector<std::string> LoadExpressionStrings(const std::string& aFilePath, int aCount);
exprtk::expression<LBM_REAL> CreateStandardExpression(const std::string& aExpressionString);
// This loads boundary conditions from file and converts them to vector of BCStructCore
// Meant to be used once per process, to load the file
std::vector<BCStructCore> LoadBoundaryConditions(const std::string& aFilePath);
// Prints out a message when there is a chance that the process work area is not big enough to cover the needs of any of the boundary
// conditions. This can happen when using the neumann bc, which is not local and needs the data from the neighbouring nodes.
void CheckBoundaryConditions(const std::vector<BCStructCore>& aBCsForProcess, const RangeStruct& aProcessWorkRange);
// This turns the boundary conditions loaded for a process to bc for thread, i.e. its meant to be used in each thread,
// to create a local expression for this thread, and customise the range
std::vector<BCStruct> CreateBoundaryConditions(const RangeStruct& aThreadWorkRange, const std::vector<BCStructCore>& aBCsForProcess);
void SetExpressionSizeParameters(exprtk::expression<LBM_REAL>& aExpression, const ConfigStruct& aConfig);
void SetExpressionPositionParameters(exprtk::expression<LBM_REAL>& aExpression, int aX, int aY, int aZ);


/// Work array functions
int GetWorkArraySize(const RangeStruct& aProcessWorkRangeStruct, const ConfigStruct& aConfig);
int GetMacroArraySize(const RangeStruct& aProcessWorkRangeStruct);
int GetNodesTypeArraySize(const RangeStruct& aProcessWorkRangeStruct);
// Initialises the array with an extra layer to the each side, so the position indexing goes from Range.Start - 1 to Range.End + 1
void InitialiseWorkArray(LBM_REAL* (&aArray), const RangeStruct& aProcessWorkRange, const ConfigStruct& aConfig);
void InitialiseMacroArray(LBM_REAL* (&aArray), const RangeStruct& aProcessWorkRange);
void InitialiseNodesTypeArray(LatticeNodeTypeEnum* (&aArray), const RangeStruct& aProcessWorkRange);

int GetWorkArrayIndex(const int& aDirIndex, const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange, const ConfigStruct& aConfig);
int GetMacroArrayIndex(const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange);
int GetNodesTypeArrayIndex(const int& aX, const int& aY, const int& aZ, const RangeStruct& aProcessWorkRange);

void WriteMacroArrays(const LBM_REAL* const aDensity, const LBM_REAL* const aVelocityX, const LBM_REAL* const aVelocityY, const LBM_REAL* const aVelocityZ, const RangeStruct& aProcessWorkRange, const std::string aFileName, const ConfigStruct& aConfig);
std::string GetFileName(const int& aIterationNumber, const int& aProcessNumber);

/// Print functions
void PrintWorkRange(const RangeStruct& aRange);
void PrintDirection(const Direction& aDirection);
void PrintStreamingMessage(const StreamingMessageStruct& aMessage);
void PrintConfig(int aProcessCount, int aThreadCount, const ConfigStruct& aConfig);
void PrintMessage(const std::string& aMessage, int aProcessId, int aThreadId, bool aVerboseOnly, const ConfigStruct& aConfig);
