#pragma once
#include "CommSettingsStruct.h"
#include "Includes.h"
#include "ConfigStruct.h"
#include "Functions.h"
#include "ConstantsLBM.h"
#include "LatticeNodeTypeEnum.h"
#include "Constants.h"

void SetInitialStateFromExpression(LBM_REAL* const (&aArray),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig, const std::vector<std::string>& aExpressionStrings);
void SetMacroValues(LBM_REAL* const aDensity,
		LBM_REAL* const aVelocityX, LBM_REAL* const aVelocityY, LBM_REAL* const aVelocityZ,
		const LBM_REAL* const aWorkArray, const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig);
void SpecifyNodesTypeFromExpression(LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig, const std::string& aExpressionString);
void StreamLocalFull(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig);
void StreamLocalInterior(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig);
void StreamLocalBorder(const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig);
void StreamLocalOneNode(const int& aX, const int& aY, const int& aZ, const Direction* const (&aDirections),
		const LBM_REAL* const (&aSourceWorkArray), LBM_REAL* const (&aTargetWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const ConfigStruct& aConfig);
void Collision(LBM_REAL* const (&aWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange, const ConfigStruct& aConfig);
void ApplyBoundaryConditions(LBM_REAL* const (&aWorkArray), const LatticeNodeTypeEnum* const (&aNodesType),
		const RangeStruct& aProcessWorkRange, const RangeStruct& aThreadWorkRange,
		const std::vector<BCStruct>& aBCForThread,
		const ConfigStruct& aConfig);
