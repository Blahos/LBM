#include "FunctionsCommon.h"


ConfigStruct LoadConfig(const std::string& aFileDir,const std::string& aFileName)
{
	std::string lFileDir = aFileDir;
	if (lFileDir.size() > 0 && lFileDir.at(lFileDir.size() - 1) != '/') lFileDir += "/";
	std::string lFilePath = lFileDir;
	lFilePath += aFileName;
    std::fstream lFile(lFilePath.c_str(), std::ios_base::in);

    ConfigStruct lConfig;
    lConfig.IsValid = false;

    if (!lFile)
    {
        std::cout<<"Failed to open file "<<lFilePath<<" !"<<std::endl;
        return lConfig;
    }

    int lLatticeTypeInt;

    lFile >> lConfig.GridSizeX;
    lFile >> lConfig.GridSizeY;
    lFile >> lConfig.GridSizeZ;

    lFile >> lLatticeTypeInt;
    lFile >> lConfig.TimeStepCount;
    lFile >> lConfig.FileOutputStepPeriod;
    lFile >> lConfig.RelaxationParameter;
    lFile >> lConfig.VelocityCoeff;

    lFile >> lConfig.InitialConditionFilePath;
    lFile >> lConfig.BoundaryCondFilePath;
    lFile >> lConfig.WallSpecFilePath;
    int lIsVerboseInt;
    lFile >> lIsVerboseInt;
    if (lIsVerboseInt != 0 && lIsVerboseInt != 1)
    {
    	std::cout<<"Invalid value for IsVerbose parameter in the main config file!"<<std::endl;
    	exit(1);
    }
    lConfig.IsVerbose = lIsVerboseInt == 0 ? 0 : 1;
    lFile >> lConfig.StepOutputPeriod;

    lFile.close();

    if (IntToLatticeType.count(lLatticeTypeInt))
    {
		lConfig.LatticeType = IntToLatticeType.at(lLatticeTypeInt);
		lConfig.IsValid = true;
    }
    else
	{
    	std::cout<<"Invalid grid type specifier! Expected 15, 19 or 27!"<<std::endl;
	}

    lConfig.ConfigDir = lFileDir;
    return lConfig;
}

void WriteConfig(const ConfigStruct& aConfig, const std::string& aFilePath)
{
    std::fstream lFile(aFilePath.c_str(), std::ios_base::out);
    if (!lFile)
    {
        std::cout<<"Failed to open file "<<aFilePath<<" !"<<std::endl;
        return;
    }

    lFile << aConfig.GridSizeX << std::endl;
	lFile << aConfig.GridSizeY << std::endl;
	lFile << aConfig.GridSizeZ << std::endl;

	lFile << (int)aConfig.LatticeType << std::endl;
	lFile << aConfig.TimeStepCount << std::endl;
	lFile << aConfig.FileOutputStepPeriod << std::endl;
	lFile << aConfig.RelaxationParameter << std::endl;
	lFile << aConfig.VelocityCoeff << std::endl;

	lFile << aConfig.InitialConditionFilePath << std::endl;
	lFile << aConfig.BoundaryCondFilePath << std::endl;
	lFile << aConfig.WallSpecFilePath << std::endl;

	lFile << (aConfig.IsVerbose ? 1 : 0) << std::endl;

    lFile << aConfig.StepOutputPeriod << std::endl;

    std::cout<<"Config written successfully to "<<aFilePath<<". Closing the file now."<<std::endl;
    lFile.close();
}

std::vector<BCStructCore> LoadBoundaryConditions(const std::string& aFilePath)
{
    std::fstream lFile(aFilePath.c_str(), std::ios_base::in);

    std::vector<BCStructCore> lConditions;

    if (!lFile)
    {
    	std::cout<<"Could not open file "<<aFilePath<<" ! Closing application now."<<std::endl;
    	exit(1);
    }
	int lCount;

	lFile >> lCount;

	for (int i = 0; i < lCount; i++)
	{
		BCStructCore lBC;

		int lTypeInt;
		lFile >> lTypeInt;
		lBC.Type = (BCTypeEnum)lTypeInt;

		RangeStruct lRange;
		lFile >> lRange.StartX; lFile >> lRange.EndX;
		lFile >> lRange.StartY; lFile >> lRange.EndY;
		lFile >> lRange.StartZ; lFile >> lRange.EndZ;

		// BC are specified in base 0 indexing, always !!!
		lRange.StartX;
		lRange.EndX;
		lRange.StartY;
		lRange.EndY;
		lRange.StartZ;
		lRange.EndZ;

		lBC.Range = lRange;

		GridDirectionStruct lOuterNormal;
		int lONX, lONY, lONZ;
		lFile >> lONX; lFile >> lONY; lFile >> lONZ;

		lOuterNormal.X = (BaseDirection)lONX;
		lOuterNormal.Y = (BaseDirection)lONY;
		lOuterNormal.Z = (BaseDirection)lONZ;
		lBC.OuterNormal = lOuterNormal;

		// This is here twice, because after reading normal vector with >> operator, the reader still apparently stays at the previous line,
		// so the first getline makes him go to the next line, and then the expression is read
		std::string lDummy;
    	getline(lFile, lDummy);
    	getline(lFile, lBC.ExpressionString1);
    	getline(lFile, lBC.ExpressionString2);
    	getline(lFile, lBC.ExpressionString3);

		lConditions.push_back(lBC);
	}

	lFile.close();

	return lConditions;
}
void CheckBoundaryConditions(const std::vector<BCStructCore>& aBCsForProcess, const RangeStruct& aProcessWorkRange)
{
	// This is only a quick created function, not sure if the check mechanism is 100% correct
	for (int iBC = 0; iBC < aBCsForProcess.size(); iBC++)
	{
		BCStructCore lBC = aBCsForProcess.at(iBC);
		if (lBC.Type != BCTypeEnum::NormalDensityGradient) continue;

		// The ranges lower bounds, in respect to the bc outer normal direction
		// The assigment varies depending on the normal direction
		int lRangeStart1;
		int lRangeStart2;
		if (lBC.OuterNormal.X != BaseDirection::Straight)
		{
			if (lBC.OuterNormal.X == BaseDirection::Up)
			{
				lRangeStart1 = aProcessWorkRange.StartX;
				lRangeStart2 = lBC.Range.StartX;
			}
			else
			{
				lRangeStart2 = aProcessWorkRange.EndX;
				lRangeStart1 = lBC.Range.EndX;
			}
		}
		if (lBC.OuterNormal.Y != BaseDirection::Straight)
		{
			if (lBC.OuterNormal.Y == BaseDirection::Up)
			{
				lRangeStart1 = aProcessWorkRange.StartY;
				lRangeStart2 = lBC.Range.StartY;
			}
			else
			{
				lRangeStart2 = aProcessWorkRange.EndY;
				lRangeStart1 = lBC.Range.EndY;
			}
		}
		if (lBC.OuterNormal.Z != BaseDirection::Straight)
		{
			if (lBC.OuterNormal.Z == BaseDirection::Up)
			{
				lRangeStart1 = aProcessWorkRange.StartZ;
				lRangeStart2 = lBC.Range.StartZ;
			}
			else
			{
				lRangeStart2 = aProcessWorkRange.EndZ;
				lRangeStart1 = lBC.Range.EndZ;
			}
		}

		if (lRangeStart2 - lRangeStart1 < 2)
			throw std::runtime_error("One of the processes does not have a big enough range to correctly manage the Neumann boundary condition! Try to set less MPI processes.");
	}
}
void WriteBoundaryConditions(const std::vector<BCStructCore>& aConditions, const std::string& aFilePath)
{
    std::fstream lFile(aFilePath.c_str(), std::ios_base::out);

    if (!lFile)
    {
    	std::cout<<"Could not open file "<<aFilePath<<" !"<<std::endl;
    	return;
    }
	int lCount = aConditions.size();

	lFile << lCount << std::endl;

	for (int i = 0; i < lCount; i++)
	{
		BCStructCore lBC = aConditions.at(i);

		int lTypeInt = (int)lBC.Type;
		lFile << lTypeInt << std::endl;

		// BC are specified in base 0 indexing, always !!!
		RangeStruct lRange = lBC.Range;
		lFile << lRange.StartX << " "; lFile << lRange.EndX << std::endl;
		lFile << lRange.StartY << " "; lFile << lRange.EndY << std::endl;
		lFile << lRange.StartZ << " "; lFile << lRange.EndZ << std::endl;

		GridDirectionStruct lOuterNormal = lBC.OuterNormal;
		lFile << lOuterNormal.X << " " << lOuterNormal.Y << " " << lOuterNormal.Z << std::endl;

		lFile << lBC.ExpressionString1 << std::endl;
		lFile << lBC.ExpressionString2 << std::endl;
		lFile << lBC.ExpressionString3 << std::endl;
	}

	lFile.close();
}
