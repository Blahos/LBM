struct BCStructCore;
#pragma once
#include <string>
#include "BCTypeEnum.h"
#include "RangeStruct.h"
#include "GridDirectionStruct.h"

struct BCStructCore
{
public:
	BCTypeEnum Type;
	RangeStruct Range;
	GridDirectionStruct OuterNormal;

	// Density or density derivative or velocity in the outer normal direction
	std::string ExpressionString1;
	// Tangent velocity in the first non normal direction (in order X, Y, Z)
	std::string ExpressionString2;
	// Tangent velocity in the second non normal direction (in order X, Y, Z)
	std::string ExpressionString3;
};
