
struct StreamingMessageStruct;
#pragma once
#include "Includes.h"
#include "RangeStruct.h"

struct StreamingMessageStruct
{
public:
	int DirectionIndex;
	RangeStruct DataRange;
	// This could be calculated from DataRange, but its more effective to store it here, so it
	// does not need to be calculated over and over again.
	// But one has to be careful to keep the structure values consistent.
	int DataSize;
};
