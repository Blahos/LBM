struct ConfigStruct;

#pragma once
#include <string>
#include "LatticeTypeEnum.h"
#include "TypesCommon.h"

struct ConfigStruct
{
public:
	// Number of nodes in X, Y and Z directions
    int GridSizeX = 0;
    int GridSizeY = 0;
    int GridSizeZ = 0;

    LatticeTypeEnum LatticeType = LatticeTypeEnum::D3Q15;

    unsigned long long TimeStepCount = 0;
    int FileOutputStepPeriod = 1;
    LBM_REAL RelaxationParameter = 1;
    LBM_REAL VelocityCoeff = 1; // Velocity 1 in LBM units mean this value in m/s

    std::string InitialConditionFilePath = "";
    std::string BoundaryCondFilePath = "";
    std::string WallSpecFilePath = "";
    bool IsVerbose = false;
    int StepOutputPeriod = 1;

    bool IsValid = false;

    std::string ConfigDir = ""; // this is not loaded from the file, but set during loading by program
};
