#pragma once
#include "Includes.h"
#include "LatticeTypeEnum.h"
#include "DirectionStruct.h"

static const std::map<LatticeTypeEnum, Direction*> CreateDirections()
{
	std::map<LatticeTypeEnum, Direction*> lMap;

	lMap[LatticeTypeEnum::D2Q9]  = new Direction[LatticeTypeEnum::D2Q9 ];
	lMap[LatticeTypeEnum::D3Q15] = new Direction[LatticeTypeEnum::D3Q15];
	lMap[LatticeTypeEnum::D3Q19] = new Direction[LatticeTypeEnum::D3Q19];
	lMap[LatticeTypeEnum::D3Q27] = new Direction[LatticeTypeEnum::D3Q27];

	std::map<LatticeTypeEnum, int> lCurrentIndex;

	lCurrentIndex[LatticeTypeEnum::D2Q9]  = 0;
	lCurrentIndex[LatticeTypeEnum::D3Q15] = 0;
	lCurrentIndex[LatticeTypeEnum::D3Q19] = 0;
	lCurrentIndex[LatticeTypeEnum::D3Q27] = 0;

	for (int iX = -1; iX <= 1; iX++)
	{
		for (int iY = -1; iY <= 1; iY++)
		{
			for (int iZ = -1; iZ <= 1; iZ++)
			{
				int lSquareSize = std::abs(iX) + std::abs(iY) + std::abs(iZ);
				// D2Q9
				if (lSquareSize <= 2 && iZ == 0)
				{
					int lIndex = lCurrentIndex[LatticeTypeEnum::D2Q9]++;
					Direction* lDir = &lMap[LatticeTypeEnum::D2Q9][lIndex];

					lDir->GridDirection.X = (BaseDirection)iX; lDir->GridDirection.Y = (BaseDirection)iY; lDir->GridDirection.Z = (BaseDirection)iZ;
					lDir->X = iX; lDir->Y = iY; lDir->Z = iZ;

					if (lSquareSize == 0) lDir->Weight = 4.0/9;
					else if (lSquareSize == 1) lDir->Weight = 1.0/9;
					else lDir->Weight = 1.0/36;
				}
				// D3Q15
				if (lSquareSize <= 1 || lSquareSize == 3)
				{
					int lIndex = lCurrentIndex[LatticeTypeEnum::D3Q15]++;
					Direction* lDir = &lMap[LatticeTypeEnum::D3Q15][lIndex];

					lDir->GridDirection.X = (BaseDirection)iX; lDir->GridDirection.Y = (BaseDirection)iY; lDir->GridDirection.Z = (BaseDirection)iZ;
					lDir->X = iX; lDir->Y = iY; lDir->Z = iZ;

					if (lSquareSize == 0) lDir->Weight = 2.0/9;
					else if (lSquareSize == 1) lDir->Weight = 1.0/9;
					else lDir->Weight = 1.0/72;
				}
				// D3Q19
				if (lSquareSize <= 2)
				{
					int lIndex = lCurrentIndex[LatticeTypeEnum::D3Q19]++;
					Direction* lDir = &lMap[LatticeTypeEnum::D3Q19][lIndex];

					lDir->GridDirection.X = (BaseDirection)iX; lDir->GridDirection.Y = (BaseDirection)iY; lDir->GridDirection.Z = (BaseDirection)iZ;
					lDir->X = iX; lDir->Y = iY; lDir->Z = iZ;

					if (lSquareSize == 0) lDir->Weight = 1.0/3;
					else if (lSquareSize == 1) lDir->Weight = 1.0/18;
					else lDir->Weight = 1.0/36;
				}
				// D3Q27
				if (true)
				{
					int lIndex = lCurrentIndex[LatticeTypeEnum::D3Q27]++;
					Direction* lDir = &lMap[LatticeTypeEnum::D3Q27][lIndex];

					lDir->GridDirection.X = (BaseDirection)iX; lDir->GridDirection.Y = (BaseDirection)iY; lDir->GridDirection.Z = (BaseDirection)iZ;
					lDir->X = iX; lDir->Y = iY; lDir->Z = iZ;

					if (lSquareSize == 0) lDir->Weight = 8.0/27;
					else if (lSquareSize == 1) lDir->Weight = 2.0/27;
					else if (lSquareSize == 2) lDir->Weight = 1.0/54;
					else lDir->Weight = 1.0/216;
				}
			}
		}
	}

	// Set opposite indices
	for (std::map<LatticeTypeEnum, Direction*>::const_iterator nIterator = lMap.begin(); nIterator != lMap.end(); nIterator++)
	{
		const LatticeTypeEnum lType = nIterator->first;
		Direction* const lDirections = nIterator->second;

		for (int iDir = 0; iDir < lType; iDir++)
		{
			Direction* const lDirection = lDirections + iDir;
			for (int iDirOpp = 0; iDirOpp < lType; iDirOpp++)
			{
				Direction lDirectionOpposite = lDirections[iDirOpp];

				if (
						lDirection->GridDirection.X == -(lDirectionOpposite.GridDirection.X) &&
						lDirection->GridDirection.Y == -(lDirectionOpposite.GridDirection.Y) &&
						lDirection->GridDirection.Z == -(lDirectionOpposite.GridDirection.Z)
					)
				{
					lDirection->OppositeDirIndex = iDirOpp;
					break;
				}
			}
		}
	}

	return lMap;
}

static const std::map<LatticeTypeEnum, Direction*> LatticeTypeToDirections
	= CreateDirections();

static const std::map<LatticeTypeEnum, LBM_REAL> LatticeTypeToDeltaCoeff1 =
{
		{ LatticeTypeEnum::D2Q9,  (LBM_REAL)1/2 },
		{ LatticeTypeEnum::D3Q15, (LBM_REAL)1/4 },
		{ LatticeTypeEnum::D3Q19, (LBM_REAL)1/2 },
		{ LatticeTypeEnum::D3Q27, (LBM_REAL)1/6 },
};

static const std::map<LatticeTypeEnum, LBM_REAL> LatticeTypeToDeltaCoeff2 =
{
		{ LatticeTypeEnum::D2Q9,  (LBM_REAL)1/3 },
		{ LatticeTypeEnum::D3Q15, (LBM_REAL)1/6 },
		{ LatticeTypeEnum::D3Q19, (LBM_REAL)1/3 },
		{ LatticeTypeEnum::D3Q27, (LBM_REAL)1/9 },
};
