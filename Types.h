#pragma once
#include "Includes.h"
#include "TypesCommon.h"

#define LBM_REAL_MPI MPI_DOUBLE
#define LBM_REAL_VTK VTK_DOUBLE
typedef vtkDoubleArray LBM_VTK_ARRAY;
