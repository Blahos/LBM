#include "FunctionsParallel.h"


void IntroduceProcess()
{
    int lWorldRank = GetProcessId();
    std::cout<<"Process with ID "<<lWorldRank<<" started."<<std::endl;
}
int GetProcessId()
{
    int lId;
    MPI_Comm_rank(MPI_COMM_WORLD, &lId);
    return lId;
}
int GetProcessCount()
{
    int lMPIProcesses;
    MPI_Comm_size(MPI_COMM_WORLD, &lMPIProcesses);
    return lMPIProcesses;
}
int GetThreadId()
{
	int lId = omp_get_thread_num();
	return lId;
}
int GetThreadCount()
{
	int lThreadCount = omp_get_num_threads();
	return lThreadCount;
}
int GetThreadCountRunOpenMP()
{
	int lThreadCount;
	int lThreadId;
#pragma omp parallel private(lThreadId)
	{
		int lThreadId = GetThreadId();
		if (lThreadId == 0) lThreadCount = GetThreadCount();
	}
	return lThreadCount;
}

CommSettingsStruct GetCommSettings(const ProcessNeighbourhoodStruct& aProcessNeighbourhood, const RangeStruct& aProcessWorkRange,
		const ConfigStruct& aConfig)
{
	CommSettingsStruct lCommSettings;
	const std::map<GridDirectionStruct, int>* lMap = &(aProcessNeighbourhood.GridDirectionToProcess);
	for (std::map<GridDirectionStruct, int>::const_iterator nIterator = lMap->begin(); nIterator != lMap->end(); nIterator++)
	{
		const GridDirectionStruct* lGridDir = &(nIterator->first);
		const int lProcessNumber = nIterator->second;

		// Determine the data range for the messages
		RangeStruct lMessageRange(aProcessWorkRange);
		if (lGridDir->X == 1) lMessageRange.StartX = lMessageRange.EndX;
		if (lGridDir->X == -1) lMessageRange.EndX = lMessageRange.StartX;
		if (lGridDir->Y == 1) lMessageRange.StartY = lMessageRange.EndY;
		if (lGridDir->Y == -1) lMessageRange.EndY = lMessageRange.StartY;
		if (lGridDir->Z == 1) lMessageRange.StartZ = lMessageRange.EndZ;
		if (lGridDir->Z == -1) lMessageRange.EndZ = lMessageRange.StartZ;

		// Check, which grid directions will be sent to the given neighbour process,
		// and which will be received from it
		for (int iDir = 0; iDir < aConfig.LatticeType; iDir++)
		{
			const Direction* const lDir = LatticeTypeToDirections.at(aConfig.LatticeType) + iDir;
			if (lGridDir->X != 0 && lGridDir->X != lDir->GridDirection.X) continue;
			if (lGridDir->Y != 0 && lGridDir->Y != lDir->GridDirection.Y) continue;
			if (lGridDir->Z != 0 && lGridDir->Z != lDir->GridDirection.Z) continue;

			int lOppositeDirIndex = lDir->OppositeDirIndex;
			const Direction* const lOppositeDir = LatticeTypeToDirections.at(aConfig.LatticeType) + lOppositeDirIndex;

			RangeStruct lMessageRangeShifted = lMessageRange.Shift(lDir->GridDirection);

			StreamingMessageStruct lMessageToSend;
			lMessageToSend.DataRange = lMessageRangeShifted;
			lMessageToSend.DirectionIndex = iDir;
			lMessageToSend.DataSize =
					(lMessageToSend.DataRange.EndX - lMessageToSend.DataRange.StartX + 1) *
					(lMessageToSend.DataRange.EndY - lMessageToSend.DataRange.StartY + 1) *
					(lMessageToSend.DataRange.EndZ - lMessageToSend.DataRange.StartZ + 1);

			lCommSettings.MessagesToSend[lProcessNumber].push_back(lMessageToSend);

			StreamingMessageStruct lMessageToReceive;
			lMessageToReceive.DataRange = lMessageRange.Shift(*lGridDir).Shift(lOppositeDir->GridDirection);
			lMessageToReceive.DirectionIndex = lOppositeDirIndex;
			lMessageToReceive.DataSize =
					(lMessageToReceive.DataRange.EndX - lMessageToReceive.DataRange.StartX + 1) *
					(lMessageToReceive.DataRange.EndY - lMessageToReceive.DataRange.StartY + 1) *
					(lMessageToReceive.DataRange.EndZ - lMessageToReceive.DataRange.StartZ + 1);
			lCommSettings.MessagesToReceive[lProcessNumber].push_back(lMessageToReceive);
		}
	}

	return lCommSettings;
}

RangeStruct GetWorkRangeForThread(int aThreadId, int aThreadCount, const RangeStruct& aProcessWorkRange)
{
	// Primitive splitting, just alongside the X direction

	RangeStruct lRange;

	int lXSize = aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1;
	int lXPortionSmall = lXSize / aThreadCount;
	int lXPortionLarge = lXPortionSmall + 1;
	int lReminder = lXSize % aThreadCount;

	if (aThreadId < lReminder && lReminder > 0)
	{
		lRange.StartX = aProcessWorkRange.StartX + aThreadId * lXPortionLarge;
		lRange.EndX = lRange.StartX + lXPortionLarge - 1;
	}
	else
	{
		lRange.StartX = aProcessWorkRange.StartX
				+ lReminder * lXPortionLarge
				+ (aThreadId - lReminder) * lXPortionSmall;
		lRange.EndX = lRange.StartX + lXPortionSmall - 1;
	}
	// This condition should not even be necessary when using the new work distribution system
	// (with reminder)
	if (aThreadId == aThreadCount - 1) lRange.EndX = aProcessWorkRange.EndX;

	lRange.StartY = aProcessWorkRange.StartY;
	lRange.EndY = aProcessWorkRange.EndY;
	lRange.StartZ = aProcessWorkRange.StartZ;
	lRange.EndZ = aProcessWorkRange.EndZ;

	return lRange;
}

RangeStruct GetWorkRangeForThread2(int aThreadId, int aThreadCount, const RangeStruct& aProcessWorkRange)
{
	// Primitive splitting, just alongside the X direction
	// Assignes one less layer to the thread dealing with the boundary conditions
	// (assumes the duct flow in the x direction, i.e. this is only a quick hacked function)
	// (also assumes one node (i.e. 24 cores) mode)
	RangeStruct lRange;

	int lXSize = aProcessWorkRange.EndX - aProcessWorkRange.StartX + 1;
	int lXPortionSmall = lXSize / aThreadCount;
	int lXPortionLarge = lXPortionSmall + 1;
	int lReminder = lXSize % aThreadCount;

	if (aThreadId < lReminder && lReminder > 0)
	{
		lRange.StartX = aProcessWorkRange.StartX + aThreadId * lXPortionLarge;
		lRange.EndX = lRange.StartX + lXPortionLarge - 1;
	}
	else
	{
		lRange.StartX = aProcessWorkRange.StartX
				+ lReminder * lXPortionLarge
				+ (aThreadId - lReminder) * lXPortionSmall;
		lRange.EndX = lRange.StartX + lXPortionSmall - 1;
	}
	// This condition should not even be necessary when using the new work distribution system
	// (with reminder)
	if (aThreadId == aThreadCount - 1) lRange.EndX = aProcessWorkRange.EndX;

	lRange.StartY = aProcessWorkRange.StartY;
	lRange.EndY = aProcessWorkRange.EndY;
	lRange.StartZ = aProcessWorkRange.StartZ;
	lRange.EndZ = aProcessWorkRange.EndZ;

	if (aThreadCount > 2)
	{
		// First and least thread get less work so they can deal with the boundary conditions
		if (aThreadId == 0) lRange.EndX--;
		if (aThreadId == 1) lRange.StartX--;
		if (aThreadId == aThreadCount - 1) lRange.StartX++;
		if (aThreadId == aThreadCount - 2) lRange.EndX++;
	}
	return lRange;
}

ProcessNeighbourhoodStruct GetProcessNeighbourhoodSimpleX(const int& aProcessId, const int& aProcessCount, const ConfigStruct& aConfig)
{
	// Primitive splitting, just alongside the X direction

	ProcessNeighbourhoodStruct lReturnStruct;
	GridDirectionStruct lBack(-1, 0, 0);
	GridDirectionStruct lForward(1, 0, 0);

	if (aProcessId > 0) lReturnStruct.GridDirectionToProcess[lBack] = aProcessId - 1;
	if (aProcessId < aProcessCount - 1) lReturnStruct.GridDirectionToProcess[lForward] = aProcessId + 1;

	return lReturnStruct;
}
RangeStruct GetWorkRangeForProcessSimpleX(int aProcessId, int aProcessCount, const ConfigStruct& aConfig)
{
	// Primitive splitting, just alongside the X direction

	RangeStruct lReturnRange;

	int lXPortionSmall = aConfig.GridSizeX / aProcessCount;
	int lXPortionLarge = lXPortionSmall + 1;
	int lReminder = aConfig.GridSizeX % aProcessCount;

	if (aProcessId < lReminder && lReminder > 0)
	{
		lReturnRange.StartX = aProcessId * lXPortionLarge;
		lReturnRange.EndX = lReturnRange.StartX + lXPortionLarge - 1;
	}
	else
	{
		lReturnRange.StartX = lReminder * lXPortionLarge + (aProcessId - lReminder) * lXPortionSmall;
		lReturnRange.EndX = lReturnRange.StartX + lXPortionSmall - 1;
	}
	if (aProcessId == aProcessCount - 1) lReturnRange.EndX = aConfig.GridSizeX - 1;

	lReturnRange.StartY = 0;
	lReturnRange.StartZ = 0;
	lReturnRange.EndY = aConfig.GridSizeY - 1;
	lReturnRange.EndZ = aConfig.GridSizeZ - 1;
	return lReturnRange;
}

ProcessNeighbourhoodStruct GetProcessNeighbourhoodOptimised(const int& aProcessId, const int& aProcessCount, const ConfigStruct& aConfig)
{
	ProcessNeighbourhoodStruct lReturnStruct;
	std::tuple<int, int, int> lSplittingSize = GetOptimalSplitting(aProcessCount, aConfig);
	int lSplittingSizeX = std::get<0>(lSplittingSize);
	int lSplittingSizeY = std::get<1>(lSplittingSize);
	int lSplittingSizeZ = std::get<2>(lSplittingSize);

	std::tuple<int, int, int> lSplittingCoord = ProcessIdToSplittingCoord(aProcessId, lSplittingSize);
	int lSplittingCoordX = std::get<0>(lSplittingCoord);
	int lSplittingCoordY = std::get<1>(lSplittingCoord);
	int lSplittingCoordZ = std::get<2>(lSplittingCoord);

	for (int iX = -1; iX <= 1; iX++)
	{
		for (int iY = -1; iY <= 1; iY++)
		{
			for (int iZ = -1; iZ <= 1; iZ++)
			{
				if (iX == 0 && iY == 0 && iZ == 0) continue;

				int lNeighbourSplittingCoordX = lSplittingCoordX + iX;
				int lNeighbourSplittingCoordY = lSplittingCoordY + iY;
				int lNeighbourSplittingCoordZ = lSplittingCoordZ + iZ;

				if (
						lNeighbourSplittingCoordX < 0 || lNeighbourSplittingCoordX >= lSplittingSizeX ||
						lNeighbourSplittingCoordY < 0 || lNeighbourSplittingCoordY >= lSplittingSizeY ||
						lNeighbourSplittingCoordZ < 0 || lNeighbourSplittingCoordZ >= lSplittingSizeZ
					)
					continue;

				int lNeighbourId = ProcessSplittingCoordToId(
						std::tuple<int, int, int>(
								lNeighbourSplittingCoordX,
								lNeighbourSplittingCoordY,
								lNeighbourSplittingCoordZ),
						lSplittingSize);
				GridDirectionStruct lGridDirection(iX, iY, iZ);
				lReturnStruct.GridDirectionToProcess[lGridDirection] = lNeighbourId;
			}
		}
	}

	return lReturnStruct;
}
RangeStruct GetWorkRangeForProcessOptimised(int aProcessId, int aProcessCount, const ConfigStruct& aConfig)
{
	RangeStruct lReturnRange;
	std::tuple<int, int, int> lSplittingSize = GetOptimalSplitting(aProcessCount, aConfig);

	if (aProcessId == 0)
	{
		std::cout<<"Process work splitting: "<<std::get<0>(lSplittingSize)<<", "<<std::get<1>(lSplittingSize)<<", "<<std::get<2>(lSplittingSize)<<std::endl;
	}

	int lSplittingSizeX = std::get<0>(lSplittingSize);
	int lSplittingSizeY = std::get<1>(lSplittingSize);
	int lSplittingSizeZ = std::get<2>(lSplittingSize);

	std::tuple<int, int, int> lSplittingCoord = ProcessIdToSplittingCoord(aProcessId, lSplittingSize);
	int lSplittingCoordX = std::get<0>(lSplittingCoord);
	int lSplittingCoordY = std::get<1>(lSplittingCoord);
	int lSplittingCoordZ = std::get<2>(lSplittingCoord);

	// X range
	int lXPortionSmall = aConfig.GridSizeX / lSplittingSizeX;
	int lXPortionLarge = lXPortionSmall + 1;
	int lXReminder = aConfig.GridSizeX % lSplittingSizeX;

	if (lSplittingCoordX < lXReminder && lXReminder > 0)
	{
		lReturnRange.StartX = lSplittingCoordX * lXPortionLarge;
		lReturnRange.EndX = lReturnRange.StartX + lXPortionLarge - 1;
	}
	else
	{
		lReturnRange.StartX = lXReminder * lXPortionLarge + (lSplittingCoordX - lXReminder) * lXPortionSmall;
		lReturnRange.EndX = lReturnRange.StartX + lXPortionSmall - 1;
	}
	if (lSplittingCoordX == lSplittingSizeX - 1) lReturnRange.EndX = aConfig.GridSizeX - 1;


	// Y range
	int lYPortionSmall = aConfig.GridSizeY / lSplittingSizeY;
	int lYPortionLarge = lYPortionSmall + 1;
	int lYReminder = aConfig.GridSizeY % lSplittingSizeY;

	if (lSplittingCoordY < lYReminder && lYReminder > 0)
	{
		lReturnRange.StartY = lSplittingCoordY * lYPortionLarge;
		lReturnRange.EndY = lReturnRange.StartY + lYPortionLarge - 1;
	}
	else
	{
		lReturnRange.StartY = lYReminder * lYPortionLarge + (lSplittingCoordY - lYReminder) * lYPortionSmall;
		lReturnRange.EndY = lReturnRange.StartY + lYPortionSmall - 1;
	}
	if (lSplittingCoordY == lSplittingSizeY - 1) lReturnRange.EndY = aConfig.GridSizeY - 1;


	// Z range
	int lZPortionSmall = aConfig.GridSizeZ / lSplittingSizeZ;
	int lZPortionLarge = lZPortionSmall + 1;
	int lZReminder = aConfig.GridSizeZ % lSplittingSizeZ;

	if (lSplittingCoordZ < lZReminder && lZReminder > 0)
	{
		lReturnRange.StartZ = lSplittingCoordZ * lZPortionLarge;
		lReturnRange.EndZ = lReturnRange.StartZ + lZPortionLarge - 1;
	}
	else
	{
		lReturnRange.StartZ = lYReminder * lZPortionLarge + (lSplittingCoordZ - lZReminder) * lZPortionSmall;
		lReturnRange.EndZ = lReturnRange.StartZ + lZPortionSmall - 1;
	}
	if (lSplittingCoordZ == lSplittingSizeZ - 1) lReturnRange.EndZ = aConfig.GridSizeZ - 1;


	return lReturnRange;
}
std::tuple<int, int, int> GetOptimalSplitting(const int& aProcessCount, const ConfigStruct& aConfig)
{
	int lOptimalForX = 0;
	int lOptimalForY = 0;
	int lOptimalForZ = 0;
	double lMinCommSize = std::numeric_limits<double>::max();

	// Brute force search
	for (int iX = 1; iX <= aProcessCount; iX++)
	{
		for (int iY = 1; iY <= aProcessCount; iY++)
		{
			for (int iZ = 1; iZ <= aProcessCount; iZ++)
			{
				int lProduct = iX * iY * iZ;
				if (lProduct != aProcessCount) continue;

				double lXLength = (double)(aConfig.GridSizeX) / iX;
				double lYLength = (double)(aConfig.GridSizeY) / iY;
				double lZLength = (double)(aConfig.GridSizeZ) / iZ;

				// In case of splitting only into 2 cells along a certain directions the whole
				// cube will never be communicated (at least one of its sides will be the domain
				// boundary). But this is only an approximation.
				double lCommSize = 2 * (lXLength * lYLength + lXLength * lZLength + lYLength * lZLength);
				if (lCommSize < lMinCommSize)
				{
					lMinCommSize = lCommSize;
					lOptimalForX = iX;
					lOptimalForY = iY;
					lOptimalForZ = iZ;
				}
			}
		}
	}

	return std::tuple<int, int, int>(lOptimalForX, lOptimalForY, lOptimalForZ);
}

int ProcessSplittingCoordToId(const std::tuple<int, int, int>& aSplittingCoord, const std::tuple<int, int, int>& aSplittingSize)
{
	int lProcessId =
			std::get<0>(aSplittingSize) * std::get<1>(aSplittingSize) * std::get<2>(aSplittingCoord) +
			std::get<0>(aSplittingSize) * std::get<1>(aSplittingCoord) +
			std::get<0>(aSplittingCoord);
	return lProcessId;
}
std::tuple<int, int, int> ProcessIdToSplittingCoord(const int& aProcessId, const std::tuple<int, int, int>& aSplittingSize)
{
	int lX = aProcessId % std::get<0>(aSplittingSize);
	int lY = (aProcessId / std::get<0>(aSplittingSize)) % std::get<1>(aSplittingSize);
	int lZ = (aProcessId / std::get<0>(aSplittingSize)) / std::get<1>(aSplittingSize);

	return std::tuple<int, int, int>(lX, lY, lZ);
}
bool CheckMPIError(int aError, const std::string& aErrorMessage)
{
    if (aError != MPI_SUCCESS)
    {
        std::cout<<"Error occured, code: \""<<aError<<"\", message: \""<<aErrorMessage<<"\""<<std::endl;
        return false;
    }
    return true;
}
void CheckMPI(int aError, const std::string& aErrorMessage)
{
	bool lSuccess = CheckMPIError(aError, aErrorMessage);
	if (!lSuccess) exit(EXIT_FAILURE);
}

