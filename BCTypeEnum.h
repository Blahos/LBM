#pragma ocne
enum BCTypeEnum
{
	// What is prescribed at the boundary, i.e. what is KNOWN
	Density = 0, NormalVelocity = 1, NormalDensityGradient = 2
};
