
// Used for functions, that could also be in the standard "Functions.h" file,
// but are meant to be used in other projects as well
// (like Problem_generator), so they can be accessed easily
// (this file will include only minimal necessary includes)
#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "ConfigStruct.h"
#include "BCStructCore.h"

ConfigStruct LoadConfig(const std::string& aFileDir,const std::string& aFileName);
void WriteConfig(const ConfigStruct& aConfig, const std::string& aFilePath);
// This loads boundary conditions from file and converts them to vector of BCStructCore
// Meant to be used once per process, to load the file
std::vector<BCStructCore> LoadBoundaryConditions(const std::string& aFilePath);
void WriteBoundaryConditions(const std::vector<BCStructCore>& aConditions, const std::string& aFilePath);
