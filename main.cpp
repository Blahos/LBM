#include "main.h"

int main(int aArgc, char *aArgv[])
{
	ConfigStruct lConfig;
    // Initialize the MPI environment
    MPI_Init(nullptr, nullptr);

    /// Initialising code
    int lProcessId = GetProcessId();
    int lProcessCount = GetProcessCount();

    std::string lProcessIdString = std::to_string(lProcessId);

    // Number of the standard working threads. Does not include the extra thread for handling mpi communication.
    int lWorkThreadCount = GetThreadCountRunOpenMP();

    lConfig = LoadConfig("./config", MainConfigName);

    if (lProcessId == 0)
	{
    	std::cout<<"---------------------------------------------------------------------"<<std::endl;
    	std::cout<<"---------------------------------------------------------------------"<<std::endl;
		std::cout<<"LBM program starting"<<std::endl;
		std::cout<<"---------------------------------------------------------------------"<<std::endl;

		PrintConfig(lProcessCount, lWorkThreadCount, lConfig);

		std::cout<<"---------------------------------------------------------------------"<<std::endl;
		std::cout<<"---------------------------------------------------------------------"<<std::endl;
	}

    CheckMPI(MPI_Barrier(MPI_COMM_WORLD));

    PrintMessage("Waiting for debugger ... done", lProcessId, -1, true, lConfig);
    PrintMessage("Process started", lProcessId, -1, true, lConfig);

    if (!lConfig.IsValid)
    {
        std::cout<<"Error while loading and distributing config file!"<<std::endl;
        return 1;
    }

    PrintMessage("Process has got a valid config", lProcessId, -1, true, lConfig);

    ProcessNeighbourhoodStruct lProcessNeighbourhood;
    CommSettingsStruct lCommSettings;
    RangeStruct lProcessWorkRange;

    // The working arrays are per process
    LBM_REAL* lCurrentArray = nullptr;
    LBM_REAL* lNextArray = nullptr;

    PrintMessage("Setting communication and work parameters ... ", lProcessId, -1, true, lConfig);
	/// Communication and work settings
    lProcessNeighbourhood = GetProcessNeighbourhoodSimpleX(lProcessId, lProcessCount, lConfig);
	lProcessWorkRange = GetWorkRangeForProcessSimpleX(lProcessId, lProcessCount, lConfig);
	lCommSettings = GetCommSettings(lProcessNeighbourhood, lProcessWorkRange, lConfig);
    PrintMessage("Setting communication and work parameters ... done", lProcessId, -1, true, lConfig);

    PrintMessage("Initialising work arrays ... ", lProcessId, -1, true, lConfig);
	InitialiseWorkArray(lCurrentArray, lProcessWorkRange, lConfig);
	InitialiseWorkArray(lNextArray, lProcessWorkRange, lConfig);
	PrintMessage("Initialising work arrays ... done", lProcessId, -1, true, lConfig);

	// Arrays for the macroscopic values
	LBM_REAL* lDensity;
	LBM_REAL* lVelocityX;
	LBM_REAL* lVelocityY;
	LBM_REAL* lVelocityZ;
	LatticeNodeTypeEnum* lNodesType;

	PrintMessage("Loading secondary config files ...", lProcessId, -1, true, lConfig);
	std::string lWallExpressionString = LoadExpressionString(lConfig.ConfigDir + lConfig.WallSpecFilePath);
	std::vector<std::string> lInitCondExpressionStrings = LoadExpressionStrings(lConfig.ConfigDir + lConfig.InitialConditionFilePath, 4);
	std::vector<BCStructCore> lBCForProcess = LoadBoundaryConditions(lConfig.ConfigDir + lConfig.BoundaryCondFilePath);
	PrintMessage("Loading secondary config files ... done", lProcessId, -1, true, lConfig);

	PrintMessage("Checking boundary conditions ...", lProcessId, -1, true, lConfig);
	CheckBoundaryConditions(lBCForProcess, lProcessWorkRange);
	PrintMessage("Checking boundary conditions ... done", lProcessId, -1, true, lConfig);

	PrintMessage("Initialising macro arrays ... ", lProcessId, -1, true, lConfig);
	InitialiseMacroArray(lDensity, lProcessWorkRange);
	InitialiseMacroArray(lVelocityX, lProcessWorkRange);
	InitialiseMacroArray(lVelocityY, lProcessWorkRange);
	InitialiseMacroArray(lVelocityZ, lProcessWorkRange);
	InitialiseNodesTypeArray(lNodesType, lProcessWorkRange);
	PrintMessage("Initialising macro arrays ... done", lProcessId, -1, true, lConfig);

    /// OpenMP parallelisation

	PrintMessage("Process entering omp parallel section", lProcessId, -1, true, lConfig);

#pragma omp parallel
    {
    	int lThreadId = GetThreadId();

    	PrintMessage("OpenMP thread started", lProcessId, lThreadId, true, lConfig);

        RangeStruct lThreadWorkRange;
        std::vector<BCStruct> lBCForThread;

        if (lThreadId < lWorkThreadCount)
        {
        	lThreadWorkRange = GetWorkRangeForThread(lThreadId, lWorkThreadCount, lProcessWorkRange);
        	lBCForThread = CreateBoundaryConditions(lThreadWorkRange, lBCForProcess);
        }
        /////////////////////
        /// Main LBM code
        /////////////////////
        PrintMessage("Main LBM code starts", lProcessId, lThreadId, true, lConfig);

    	// All working threads need macro arrays to be ready before setting their initial values

        if (lThreadId < lWorkThreadCount)
        {
			// Initialise the assigned part of the working array
			PrintMessage("Setting initial state ...", lProcessId, lThreadId, true, lConfig);
			SetInitialStateFromExpression(lCurrentArray, lProcessWorkRange, lThreadWorkRange, lConfig, lInitCondExpressionStrings);
			PrintMessage("Setting initial state ... done", lProcessId, lThreadId, true, lConfig);
        }

#pragma omp barrier

        if (lThreadId < lWorkThreadCount)
        {
			PrintMessage("Setting nodes type ...", lProcessId, lThreadId, true, lConfig);
			SpecifyNodesTypeFromExpression(lNodesType, lProcessWorkRange, lThreadWorkRange, lConfig, lWallExpressionString);
			PrintMessage("Setting nodes type ... done", lProcessId, lThreadId, true, lConfig);
        }

#pragma omp barrier

        if (lThreadId < lWorkThreadCount)
        {
			PrintMessage("Setting macro values ... ", lProcessId, lThreadId, true, lConfig);
			SetMacroValues(lDensity, lVelocityX, lVelocityY, lVelocityZ,
					lCurrentArray, lNodesType, lProcessWorkRange, lThreadWorkRange, lConfig);
			PrintMessage("Setting macro values ... done", lProcessId, lThreadId, true, lConfig);
        }

    	// Main thread needs to wait for all threads to finish their part of macro arrays initialisation
    	// so they can be written into a file

#pragma omp barrier
    	if (lThreadId == 0 && lConfig.FileOutputStepPeriod > 0)
    	{
    		PrintMessage("Writing initial macro values", lProcessId, lThreadId, true, lConfig);

    		std::string lFileName = GetFileName(0, lProcessId);
    		// Write macro values into a vti file
    		WriteMacroArrays(lDensity, lVelocityX, lVelocityY, lVelocityZ, lProcessWorkRange, lFileName, lConfig);
    	}

    	PrintMessage("Starting the main loop", lProcessId, lThreadId, true, lConfig);

    	auto lStartTimeTotal = std::chrono::system_clock::now();

    	int lTimeStepCount = lConfig.TimeStepCount;
    	for (int iStep = 0; iStep < lTimeStepCount; iStep++)
    	{
    		// Operation order:
    		//	Streaming step - communication part - streaming given parts of the work array to the neigbour processes
    		//	Streaming step - local part (also half way bounce back made in case of solid wall nodes)
    		// 	Boundary conditions application - filling the rest of the missing values, coming from the boundaries
    		//	Collision step
    		//	Switch working arrays
    		// 	Output macro variables if needed

    		auto lStartTimeIter = std::chrono::system_clock::now();

    		// Barrier at the start of each step, time synchronisation

    		PrintMessage("Step " + std::to_string(iStep) + " started", lProcessId, lThreadId, true, lConfig);
    		PrintMessage("Waiting at the barriers", lProcessId, lThreadId, true, lConfig);

    		if (lThreadId == 0)
    			CheckMPI(MPI_Barrier(MPI_COMM_WORLD));

#pragma omp barrier

    		PrintMessage("Barriers crossed", lProcessId, lThreadId, true, lConfig);

    		if (lProcessId == 0 && lThreadId == 0 && (iStep + 1) % lConfig.StepOutputPeriod == 0)
    			PrintMessage("Step " + std::to_string(iStep + 1), lProcessId, lThreadId, false, lConfig);

    		PrintMessage("Step " + std::to_string(iStep) + " started", lProcessId, lThreadId, true, lConfig);

#pragma omp barrier

    		if (lThreadId < lWorkThreadCount)
    		{
				PrintMessage("Streaming values locally (full)... ", lProcessId, lThreadId, true, lConfig);
				StreamLocalFull(lCurrentArray, lNextArray, lNodesType, lProcessWorkRange, lThreadWorkRange, lConfig);
				PrintMessage("Streaming values locally (full)... done", lProcessId, lThreadId, true, lConfig);
    		}
#pragma omp barrier

    		// Communication is done at the same time as interior streaming, since these two operations dont overlap
    		if (lThreadId == 0)
    		{
    			PrintMessage("Streaming values to other processes ...", lProcessId, lThreadId, true, lConfig);
    			// Values to stream are already streamed by StreamLocal into the NextArray, therefore we need
    			// to send values from there
    			StreamComm(lNextArray, lNextArray, lNodesType, lProcessWorkRange, lConfig, lCommSettings);
    			PrintMessage("Streaming values to other processes ... done", lProcessId, lThreadId, true, lConfig);
    		}

#pragma omp barrier

    		if (lThreadId < lWorkThreadCount)
    		{
				PrintMessage("Applying boundary conditions ...", lProcessId, lThreadId, true, lConfig);
				ApplyBoundaryConditions(lNextArray, lNodesType, lProcessWorkRange, lThreadWorkRange, lBCForThread, lConfig);
				PrintMessage("Applying boundary conditions ... done", lProcessId, lThreadId, true, lConfig);
    		}

    		// This barrier is because of the possible Neumann boundary condition,
    		// which can theoretically require data from the neighbouring thread working area (it is not local)
#pragma omp barrier

    		if (lThreadId < lWorkThreadCount)
    		{
				PrintMessage("Performing collision ...", lProcessId, lThreadId, true, lConfig);
				Collision(lNextArray, lNodesType, lProcessWorkRange, lThreadWorkRange, lConfig);
				PrintMessage("Performing collision ... done", lProcessId, lThreadId, true, lConfig);
    		}
#pragma omp barrier
    		if (lThreadId == 0)
    		{
    			PrintMessage("Swapping work arrays ...", lProcessId, lThreadId, true, lConfig);
    			LBM_REAL* lTempArray = lCurrentArray;
    			lCurrentArray = lNextArray;
    			lNextArray = lTempArray;
    			PrintMessage("Swapping work arrays ... done", lProcessId, lThreadId, true, lConfig);
    		}

#pragma omp barrier
    		if (lConfig.FileOutputStepPeriod > 0 && (iStep + 1) % lConfig.FileOutputStepPeriod == 0)
			{
        		if (lThreadId < lWorkThreadCount)
        		{
					SetMacroValues(lDensity, lVelocityX, lVelocityY, lVelocityZ,
							lCurrentArray, lNodesType, lProcessWorkRange, lThreadWorkRange, lConfig);
        		}
#pragma omp barrier
				if (lThreadId == 0)
				{
					std::string lFileName = GetFileName(iStep + 1, lProcessId);

					PrintMessage("Writing macro arrays ... ", lProcessId, lThreadId, true, lConfig);
					WriteMacroArrays(lDensity, lVelocityX, lVelocityY, lVelocityZ, lProcessWorkRange, lFileName, lConfig);
					PrintMessage("Writing macro arrays ... done", lProcessId, lThreadId, true, lConfig);
				}
#pragma omp barrier
    		}

    		PrintMessage("Step " + std::to_string(iStep) + " finished", lProcessId, lThreadId, true, lConfig);

    		// Benchmark
    		if (lProcessId == 0 && lThreadId == 0 && (iStep + 1) % lConfig.StepOutputPeriod == 0)
    		{
    			auto lEnd = std::chrono::system_clock::now();
    			std::chrono::duration<double, std::milli> lElapsedTotalMs = lEnd - lStartTimeTotal;
    			std::chrono::duration<double, std::milli> lElapsedIterMs = lEnd - lStartTimeIter;

    			double lStepsPerSecondTotal = (double)(iStep + 1) / lElapsedTotalMs.count() * (double)1000.0;
    			double lStepsPerSecondIter = (double)1 / lElapsedIterMs.count() * (double)1000.0;

    			PrintMessage("Average steps per second: " + std::to_string(lStepsPerSecondTotal), lProcessId, lThreadId, false, lConfig);
    			PrintMessage("Current steps per second: " + std::to_string(lStepsPerSecondIter), lProcessId, lThreadId, false, lConfig);
    		}
    	}
    }

    /// Finalising code

    PrintMessage("Process finished", lProcessId, -1, true, lConfig);

    CheckMPI(MPI_Barrier(MPI_COMM_WORLD));

    if (lProcessId == 0) PrintMessage("LBM program terminating - success", lProcessId, -1, false, lConfig);

    // Finalize the MPI environment.
    MPI_Finalize();
    return 0;
}
