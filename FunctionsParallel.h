#pragma once
#include "Includes.h"
#include "Functions.h"
#include "ConstantsLBM.h"
#include "ProcessNeighbourhoodStruct.h"
#include "RangeStruct.h"

/// Process/thread management functions
void IntroduceProcess();
int GetProcessId();
int GetProcessCount();
int GetThreadId();
int GetThreadCount();
int GetThreadCountRunOpenMP();

/// Communication functions

CommSettingsStruct GetCommSettings(const ProcessNeighbourhoodStruct& aProcessNeighbourhood, const RangeStruct& aProcessWorkRange,
		const ConfigStruct& aConfig);
RangeStruct GetWorkRangeForThread(int aThreadId, int aThreadCount, const RangeStruct& aProcessWorkRange);
RangeStruct GetWorkRangeForThread2(int aThreadId, int aThreadCount, const RangeStruct& aProcessWorkRange);

// Simple X process work splitting
ProcessNeighbourhoodStruct GetProcessNeighbourhoodSimpleX(const int& aProcessId, const int& aProcessCount, const ConfigStruct& aConfig);
RangeStruct GetWorkRangeForProcessSimpleX(int aProcessId, int aProcessCount, const ConfigStruct& aConfig);
// Optimised process work splitting
ProcessNeighbourhoodStruct GetProcessNeighbourhoodOptimised(const int& aProcessId, const int& aProcessCount, const ConfigStruct& aConfig);
RangeStruct GetWorkRangeForProcessOptimised(int aProcessId, int aProcessCount, const ConfigStruct& aConfig);
// Number of cells along the x, y and z directions
std::tuple<int, int, int> GetOptimalSplitting(const int& aProcessCount, const ConfigStruct& aConfig);
int ProcessSplittingCoordToId(const std::tuple<int, int, int>& aSplittingCoord, const std::tuple<int, int, int>& aSplittingSize);
std::tuple<int, int, int> ProcessIdToSplittingCoord(const int& aProcessId, const std::tuple<int, int, int>& aSplittingSize);
/// MPI error check functions
bool CheckMPIError(int aError, const std::string& aErrorMessage = "");
// Checks MPI routine return error code, terminates process if its not success
void CheckMPI(int aError, const std::string& aErrorMessage = "");

